
_Zen_stack_Trace = ["Leo_fnc_SomeoneInArea", _this] call Zen_StackAdd;
private ["_unitsArray", "_polygonArgs", "_someoneInArea", "_unit", "_isInArea", "_isInBlacklist", "_blacklist", "_inAreaArgs"];

if !([_this, [["VOID"], ["VOID"], ["ARRAY"], ["SCALAR"], ["STRING"], ["ARRAY"]], [[], [], ["SCALAR", "STRING"], [], [], ["STRING"]], 2] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (false)
};

_unitsArray = [(_this select 0)] call Zen_ConvertToObjectArray;

_blacklist = [];

if (count _this > 3) then {
    _polygonArgs = [_this, 1, 4] call Zen_ArrayGetIndexedSlice;
    if (count _this > 5) then {
        _blacklist = _this select 5;
    };
} else {
    _polygonArgs = _this select 1;
    if (count _this > 2) then {
        _blacklist = _this select 2;
    };
};

_someoneInArea = false;

{
    _inAreaArgs = [];
    _unit = _x;
    0 = [_inAreaArgs, _unit, _polygonArgs] call Zen_ArrayAppendNested;
    _isInArea = _inAreaArgs call Zen_IsPointInPoly;

    if (_isInArea) exitWith {
        _someoneInArea = true;
    };

    _isInBlacklist = false;
    if (count _blacklist > 0) then {
        {
            // _isInBlacklist = [_unit, _x] call Zen_IsPointInPoly;
            _isInBlacklist = (getPosATL _unit) inArea _x;
            if (_isInBlacklist) exitWith {};
        } forEach _blacklist;
    };

    if (_isInBlacklist) exitWith {
        _someoneInArea = false;
    };
} forEach _unitsArray;

call Zen_StackRemove;
(_someoneInArea)
