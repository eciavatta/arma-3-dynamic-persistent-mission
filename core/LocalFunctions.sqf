
#include "..\Zen_FrameworkFunctions\Zen_FrameworkLibrary.sqf"

Leo_fnc_Debug = {
    _Zen_stack_Trace = ["Leo_fnc_Debug", _this] call Zen_StackAdd;
	private ["_args"];
    
    if !([_this, [["VOID"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
	_args = _this select 0;
	
	if (Leo_DebugEnabled) then {
		diag_log format ["Debug: %1", _args];
		
		if (!isDedicated) then {
			hint format ["Debug: %1", _args];
            systemChat format ["Debug: %1", _args];
		};		
	};
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_ShowNotification = {
    _Zen_stack_Trace = ["Leo_fnc_ShowNotification", _this] call Zen_StackAdd;
    private ["_template", "_arguments"];

    if !([_this, [["STRING"], ["ARRAY"]], [], 2] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _template = _this select 0;
    _arguments = _this select 1;
    0 = [_template, _arguments] call bis_fnc_showNotification;
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_InitExtendedDebug = {
    _Zen_stack_Trace = ["Leo_fnc_InitExtendedDebug", _this] call Zen_StackAdd;
    private ["_unit"];

    if !([_this, [["OBJECT"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _unit = _this select 0;
    _unit addAction [
		"<t color='#FFFF00'>Disable damage (Debug)</t>",	/* Title */
		"Leo_DisableDamage = true",		/* Script */
		[],		/* Arguments */
		1,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"!Leo_DisableDamage"		/* Condition */
	];

    _unit addAction [
		"<t color='#FFFF00'>Enable damage (Debug)</t>",	/* Title */
		"Leo_DisableDamage = false",		/* Script */
		[],		/* Arguments */
		1,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"Leo_DisableDamage"		/* Condition */
	];

    _unit addAction [
		"<t color='#FFFF00'>Ammo Box (Debug)</t>",	/* Title */
		"['Open',true] spawn BIS_fnc_arsenal",		/* Script */
		[],		/* Arguments */
		2,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];
    
    _unit addAction [
		"<t color='#FFFF00'>Nuke Objective (Debug)</t>",	/* Title */
		"[_this select 1] spawn Leo_fnc_CommandNukeObjective",		/* Script */
		[],		/* Arguments */
		0,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_CommandNukeObjective = {    
    _Zen_stack_Trace = ["Leo_fnc_CommandNukeObjective", _this] call Zen_StackAdd;
    private ["_player", "_args"];

    if !([_this, [["OBJECT"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _player = _this select 0;
    _args = [_player];
    
    ZEN_FMW_MP_REServerOnly("Leo_fnc_NukeObjective", _args, call);
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_SideChat = {    
    _Zen_stack_Trace = ["Leo_fnc_SideChat", _this] call Zen_StackAdd;
    private ["_chatText"];

    if !([_this, [["STRING"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _chatText = _this select 0;
    
    [west, "Base"] sideChat _chatText;
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_GroupChat = {    
    _Zen_stack_Trace = ["Leo_fnc_GroupChat", _this] call Zen_StackAdd;
    private ["_chatText"];

    if !([_this, [["STRING"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _chatText = _this select 0;
    
    player groupChat _chatText;
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_AddDeployTentAction = {
    _Zen_stack_Trace = ["Leo_fnc_AddDeployTentAction", _this] call Zen_StackAdd;
    
    player addAction [
		"<t color='#34A853'>Deploy tent</t>",	/* Title */
		"0 = [] spawn Leo_fnc_CommandDeployTent",		/* Script */
		false,		/* Arguments */
		6,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];
        
    call Zen_StackRemove;
    (0)
};

Leo_fnc_CommandDeployTent = {
    _Zen_stack_Trace = ["Leo_fnc_CommandDeployTent", _this] call Zen_StackAdd;
    private ["_args"];
    
    _args = [player];
    ZEN_FMW_MP_REServerOnly("Leo_fnc_DeployTent", _args, call);
        
    call Zen_StackRemove;
    (0)
};

Leo_fnc_AddTentTeleportAction = {
    _Zen_stack_Trace = ["Leo_fnc_AddTentTeleportAction", _this] call Zen_StackAdd;
    
    player addAction [
		"<t color='#FBBC05'>Teleport to tent</t>",	/* Title */
		"0 = [] spawn Leo_fnc_CommandTentTeleport",		/* Script */
		false,		/* Arguments */
		6,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];
        
    call Zen_StackRemove;
    (0)
};

Leo_fnc_CommandTentTeleport = {
    _Zen_stack_Trace = ["Leo_fnc_CommandTentTeleport", _this] call Zen_StackAdd;
    private ["_args"];
    
    _args = [player];
    ZEN_FMW_MP_REServerOnly("Leo_fnc_TeleportPlayer", _args, call);
        
    call Zen_StackRemove;
    (0)
};

Leo_fnc_ExecuteTeleport = {
    _Zen_stack_Trace = ["Leo_fnc_ExecuteTeleport", _this] call Zen_StackAdd;
    private ["_pos"];

    if !([_this, [["ARRAY"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _pos = _this select 0;

    titleText ["You are about to be teleported to the tent..", "BLACK", 5];
	sleep 5;
	player setPos _pos;
	titleFadeOut 1;	
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_UpdateLocalRespawnPoints = {
    _Zen_stack_Trace = ["Leo_fnc_UpdateLocalRespawnPoints", _this] call Zen_StackAdd;
    private ["_respawnPoints"];
    
    if !([_this, [["ARRAY"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _respawnPoints = _this select 0;
    INS_REV_CFG_list_of_respawn_locations_blufor = _respawnPoints;
    
    call Zen_StackRemove;
    (0)
};



LEO_VAR_respawnPoints = [];
"LEO_VAR_respawnPoints" addPublicVariableEventHandler {
	INS_REV_CFG_list_of_respawn_locations_blufor = _this select 1;	
};

LEO_VAR_vehicleMHQRespawn = objNull;
"LEO_VAR_vehicleMHQRespawn" addPublicVariableEventHandler {
	[_this select 1] spawn LEO_SF_addMHQActions;
};

LEO_VAR_respawnVirtualArsenal = objNull;
"LEO_respawnVirtualArsenal" addPublicVariableEventHandler {
	(_this select 1) addAction [
		"<t color='#4285F4'>Virtual Arsenal</t>",	/* Title */
		"['Open', true] spawn BIS_fnc_arsenal",		/* Script */
		true,		/* Arguments */
		9.1,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];
};

