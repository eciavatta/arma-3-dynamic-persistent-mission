
_Zen_stack_Trace = ["Leo_fnc_InactiveObjectiveTask", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePos", "_objectiveRadius", "_triggerSize", "_cacheId", "_spawnedObjects", "_currentTaskHandle"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

_objectiveId = _this select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;
_triggerSize = [_objectiveRadius + Leo_ExtendedRadius, _objectiveRadius + Leo_ExtendedRadius];

_cacheId = ((Leo_PrimaryObjectivesInfo select _objectiveId) select 2);
_spawnedObjects = [];
 
while {true} do {    
	if ([Leo_AlivePlayers, _objectivePos, _triggerSize, 0, "ellipse"] call Leo_fnc_SomeoneInArea) exitWith {
		if (count _cacheId > 0) then {
            //_spawnedObjects = [_objectiveId] call Leo_fnc_LoadObjectiveFromCache;
		} else {
			_spawnedObjects = [_objectiveId] call Leo_fnc_CreateObjective;
		};
		
		_currentTaskHandle = [_objectiveId] spawn Leo_fnc_ActiveObjectiveTask;
		        
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [0, _currentTaskHandle];
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [1, _spawnedObjects select 0];
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [3, _spawnedObjects select 1];
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [4, _spawnedObjects select 2];
        (Leo_PrimaryObjectivesInfo select _objectiveId) set [5, 1];
				
		call Zen_StackRemove; 
		(0)
	};
	
	sleep (Leo_TriggerInterval / 10);
};
