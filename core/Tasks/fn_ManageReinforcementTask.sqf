
_Zen_stack_Trace = ["Leo_fnc_ManageReinforcementTask", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveName", "_objectivePos", "_objectiveSize", "_objectiveMarker",
         "_reinforcementsTypes", "_reinforcementHandle", "_vehicle", "_group", "_trackVehicle", "_trackInfantry", "_reinforcementType", "_objects"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

_objectiveId = _this select 0;
_objectiveName = (Leo_PrimaryObjectiveList select _objectiveId) select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveMarker = [_objectiveId] call Leo_fnc_GetMarker;

_reinforcementsTypes = [];
switch (_objectiveSize) do {
	case 0: {_reinforcementsTypes = Leo_ReinforcementsTypesSmallTown};
	case 1: {_reinforcementsTypes = Leo_ReinforcementsTypesMediumTown};
    case 2: {_reinforcementsTypes = Leo_ReinforcementsTypesBigTown};
};

if ([_objectiveMarker] call Zen_IsWaterArea < 0.6) then {
    [_reinforcementsTypes, "boat_insertion"] call Zen_ArrayRemoveValue;
    [_reinforcementsTypes, "boat_patrol"] call Zen_ArrayRemoveValue;
};

_reinforcementHandle = scriptNull;

_vehicle = objNull;
_group = grpNull;

_trackVehicle = [[], scriptNull];
_trackInfantry = [[], scriptNull];

while {(Leo_PrimaryObjectivesInfo select _objectiveId) select 5 == 1} do {
    _reinforcementType = [_reinforcementsTypes] call Zen_ArrayGetRandom;
    [format ["Calling reinforcement with type %1 in the city of %2", _reinforcementType, _objectiveName]] call Leo_fnc_Debug;
    
    switch (_reinforcementType) do {
        case "aircraft_patrol": {
            _objects = [_objectiveId] call Leo_fnc_AircraftPatrol;
            _vehicle = _objects select 0;
            _group = _objects select 1;
            _trackVehicle = [_vehicle] call Leo_fnc_TrackVehicles;
            
            _reinforcementHandle = [_vehicle, _objectiveMarker, [], 0, "full", "combat", Leo_HeliHeightFly, true] spawn Zen_OrderAircraftPatrol;
        };
        case "boat_insertion": {
            _group = [_objectiveId] call Leo_fnc_BoatInsertion;
            _reinforcementHandle = [_group, _objectiveMarker, [], 0, "full", "combat", true, false, false] spawn Zen_OrderInfantryPatrol;
        };
        case "boat_patrol": {
            _objects = [_objectiveId] call Leo_fnc_BoatPatrol;
            _vehicle = _objects select 0;
            _group = _objects select 1;
            _trackVehicle = [_vehicle] call Leo_fnc_TrackVehicles;
            _reinforcementHandle = [_vehicle, _objectivePos, 500, 0, "full", "combat", true] spawn Zen_OrderBoatPatrol;
        };
        case "convoy_insertion": {
            _group = [_objectiveId] call Leo_fnc_ConvoyInsertion;
            _reinforcementHandle = [_group, _objectiveMarker, [], 0, "full", "combat", true, false, false] spawn Zen_OrderInfantryPatrol;
        };
        case "heli_insertion": {
            _group = [_objectiveId, [["land", "fastrope", "parachute"]] call Zen_ArrayGetRandom] call Leo_fnc_HeliInsertion;
            _reinforcementHandle = [_group, _objectiveMarker, [], 0, "full", "combat", true, false, false] spawn Zen_OrderInfantryPatrol;
        };
        case "heli_patrol": {
            _objects = [_objectiveId] call Leo_fnc_HeliPatrol;
            _vehicle = _objects select 0;
            _group = _objects select 1;
            _trackVehicle = [_vehicle] call Leo_fnc_TrackVehicles;
            _reinforcementHandle = [_vehicle, _objectiveMarker, [], 0, "full", "combat", Leo_HeliHeightFly, true] spawn Zen_OrderAircraftPatrol;
        };
    };
    
    _trackInfantry = [_group] call Leo_fnc_TrackEnemies;
    
    waitUntil {
        sleep Leo_TriggerInterval;        
        (scriptDone _reinforcementHandle)        
    };
    
    [_trackVehicle] call Leo_fnc_RemoveTrack;
    [_trackInfantry] call Leo_fnc_RemoveTrack;      
    
    sleep Leo_CallReinforcementsAfter;
    
    [[_group, _vehicle]] call Leo_fnc_DeleteAll;
};

call Zen_StackRemove;
(0)
