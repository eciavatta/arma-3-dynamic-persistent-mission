
_Zen_stack_Trace = ["Leo_fnc_ActiveObjectiveTask", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveName", "_objectivePos", "_objectiveRadius", "_triggerSize", "_args",
         "_spawnedObjects", "_enemyUnits", "_playersAbsenseSeconds", "_pastTime", "_reinforcementsCalled", "_reinforcementsHandle", "_cacheId", "_currentTaskHandle"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

#include "..\..\Zen_FrameworkFunctions\Zen_FrameworkLibrary.sqf"

_objectiveId = _this select 0;
_objectiveName = (Leo_PrimaryObjectiveList select _objectiveId) select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;
_triggerSize = [_objectiveRadius + Leo_ExtendedRadius, _objectiveRadius + Leo_ExtendedRadius];

_spawnedObjects = (Leo_PrimaryObjectivesInfo select _objectiveId) select 1;
_enemyUnits = [_spawnedObjects select 0, (_spawnedObjects select 2) select 1] call Zen_ConvertToObjectArray;

_playersAbsenseSeconds = 0;
_pastTime = 0;
_reinforcementsCalled = false;
_reinforcementsHandle = scriptNull;
 
while {true} do {
    _enemyUnits = [_enemyUnits] call Zen_ArrayRemoveDead;

	if (count _enemyUnits == 0) exitWith {
        (Leo_PrimaryObjectivesInfo select _objectiveId) set [5, 2];
    
        if (_reinforcementsCalled) then {
            _args = ["Our UAV may have noticed movements close to the objective"];
            ZEN_FMW_MP_RENonDedicated("Leo_fnc_SideChat", _args, call);
            sleep 0.5;
            _args = ["Arm yourself using supplies left by the enemy and wait for a possible attack"];
            ZEN_FMW_MP_RENonDedicated("Leo_fnc_SideChat", _args, call);
                        
            waitUntil {
                sleep Leo_TriggerInterval;        
                (scriptDone _reinforcementsHandle)        
            };
        };        
    
        [_objectiveId] call Leo_fnc_DestroyObjective;
		
        _args = ["PrimaryObjectiveTaken", [_objectiveName]];
        ZEN_FMW_MP_RENonDedicated("Leo_fnc_ShowNotification", _args, call);
        
        (Leo_PrimaryObjectiveList select _objectiveId) set [4, true];
        [_objectiveId, true] call Leo_fnc_SetMarkerSide;
				
		call Zen_StackRemove;
		(0)
	};
    
    if ([Leo_AlivePlayers, _objectivePos, _triggerSize, 0, "ellipse"] call Leo_fnc_SomeoneInArea) then {
        _playersAbsenseSeconds = 0;    
    } else {
        _playersAbsenseSeconds = _playersAbsenseSeconds + Leo_TriggerInterval;
    };
    
    _pastTime = _pastTime + Leo_TriggerInterval;
    
    if (_playersAbsenseSeconds >= Leo_DisablePrimaryObjectiveAfterSeconds) exitWith {
        _cacheId = [_objectiveId] call Leo_fnc_SaveObjectiveToCache;
                
        _currentTaskHandle = [_objectiveId] spawn Leo_fnc_InactiveObjectiveTask;
        
        (Leo_PrimaryObjectivesInfo select _objectiveId) set [0, _currentTaskHandle];        
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [1, []];
        (Leo_PrimaryObjectivesInfo select _objectiveId) set [2, _cacheId];
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [3, []];
		(Leo_PrimaryObjectivesInfo select _objectiveId) set [4, []];
        (Leo_PrimaryObjectivesInfo select _objectiveId) set [5, 0];
        
        call Zen_StackRemove; 
		(0)
    };
    
    if (!_reinforcementsCalled && _pastTime > Leo_CallReinforcementsAfter) then {
        for "_x" from 0 to count _enemyUnits - 1 do {
            for "_y" from 0 to count Leo_AlivePlayers - 1 do {
                if ((_enemyUnits select _x) knowsAbout (Leo_AlivePlayers select _y) > 2) then {
                    if (!_reinforcementsCalled) then {
                        _reinforcementsHandle = [_objectiveId] spawn Leo_fnc_CallReinforcements;
                        _reinforcementsCalled = true;
                    };
                };
            };
        };
    };
	
	sleep Leo_TriggerInterval;
};
