
_Zen_stack_Trace = ["Leo_fnc_RandomPatrolTask", _this] call Zen_StackAdd;
private ["_spawnSearchPos", "_moveSearchPos", "_spawnPos", "_movePos", "_vehiclesClasses", "_vehicle", "_handle"];

while {true} do {
    _spawnSearchPos = ([Leo_PrimaryObjectiveList] call Zen_ArrayGetRandom) select 1;
    _moveSearchPos = ([Leo_PrimaryObjectiveList] call Zen_ArrayGetRandom) select 1;
    
    _spawnPos = [_spawnSearchPos, 0, 0, 1, [2, Leo_ScanRoadRadius]] call Zen_FindGroundPosition;
    _movePos = [_moveSearchPos, 0, 0, 1, [2, Leo_ScanRoadRadius]] call Zen_FindGroundPosition;
    
    _vehiclesClasses = [["mrap", "tank", "acp"]] call Leo_fnc_GetVehiclesClasses;    
    _vehicle = objNull;
    while {isNull _vehicle} do {
        _vehicle = [_spawnPos, _vehiclesClasses, [_spawnPos, _movePos] call Zen_FindDirection] call Zen_SpawnGroundVehicle;
        if (!alive _vehicle) then {
            {
                deleteVehicle _x;
            } forEach crew _vehicle;
            deleteVehicle _vehicle;
            _vehicle = objNull;
        };
    };
    
    0 = [_vehicle] call Leo_fnc_TrackVehicles;
    _handle = [_vehicle, _movePos, "limited", 0, true, true] spawn Zen_OrderVehicleMove;
    
    waitUntil {
        sleep Leo_TriggerInterval;
        
        (scriptDone _handle)
    };
};

call Zen_StackRemove;
(0)
