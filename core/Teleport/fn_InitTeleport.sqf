
if !(isDedicated) then {
    0 = [] spawn Leo_fnc_AddDeployTentAction;
	player addEventHandler ["Respawn", {0 = [] spawn Leo_fnc_AddDeployTentAction}];
    
    waitUntil {
        sleep 1;
        (!isNil "INS_REV_CFG_list_of_respawn_locations_blufor")    
    };
    
    [[player]] call Leo_fnc_CommandUpdateRespawnPoints;
};

if !(isServer) exitWith {
    (0)
}; 

_Zen_stack_Trace = ["Leo_fnc_InitTeleport", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveMarker", "_objectiveSize", "_ammoBoxList", "_nAmmoBox", "_centerPos",
         "_buildingName", "_buildingPositions", "_ammoBox", "_containsExplosives"];

         

Leo_GroupTentList = [];

[INS_REV_CFG_list_of_respawn_locations_blufor, "MHQ_HELI"] call Zen_ArrayAppend;
[INS_REV_CFG_list_of_respawn_locations_blufor, "MHQ_TRUCK"] call Zen_ArrayAppend;
[[]] call Leo_fnc_CommandUpdateRespawnPoints;




call Zen_StackRemove;
(0)
