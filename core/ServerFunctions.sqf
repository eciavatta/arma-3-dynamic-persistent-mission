
#include "..\Zen_FrameworkFunctions\Zen_FrameworkLibrary.sqf"

/** Use framework function Zen_TrackGroups with its arguments */
Leo_fnc_TrackEnemies = {
	if (Leo_TrackEnemies) exitWith {
		(_this call Zen_TrackGroups)
	};
	
	if (true) exitWith {
		([[], scriptNull])
	};
};

/** Use framework function Zen_TrackGroups with its arguments */
Leo_fnc_TrackVehicles = {
	if (Leo_TrackEnemies) exitWith {
		(_this call Zen_TrackVehicles)
	};
	
	if (true) exitWith {
		([[], scriptNull])
	};
};

Leo_fnc_AllowDamageServer = {
    _Zen_stack_Trace = ["Leo_fnc_AllowDamageServer", _this] call Zen_StackAdd;
    private ["_unit", "_allowDamage"];

    if !([_this, [["OBJECT"], ["BOOL"]], [], 2] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _unit = _this select 0;
    _allowDamage = _this select 1;
    
    [format ["Allow damage to %1? %2", _unit, _allowDamage]] call Leo_fnc_Debug;    
    _unit allowDamage _allowDamage;
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_NukeAll = {
    _Zen_stack_Trace = ["Leo_fnc_NukeAll", _this] call Zen_StackAdd;
    private ["_objects", "_counter"];

    if !([_this, [["VOID"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _objects = _this select 0;
    _counter = 0;
	{
		_x setDamage 1;
        _counter = _counter + 1;
	} forEach ([_objects] call Zen_ConvertToObjectArray);
    
    [format ["%1 objects nuked", _counter]] call Leo_fnc_Debug;
    
    call Zen_StackRemove;
    (_counter)
};

Leo_fnc_NukeObjective = {
    _Zen_stack_Trace = ["Leo_fnc_NukeObjective", _this] call Zen_StackAdd;
    private ["_player", "_objectiveId", "_message", "_args"];

    if !([_this, [["OBJECT"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _player = _this select 0;
    _objectiveId = -1;
    {
        if ([_player, _x select 1, [_x select 3, _x select 3], 0, "ellipse"] call Zen_AreInArea) then {
            _objectiveId = _forEachIndex;
        };
    } forEach Leo_PrimaryObjectiveList;
    
    _message = "";
    if (_objectiveId >= 0) then {
        _result = [(Leo_PrimaryObjectivesInfo select _objectiveId) select 1] call Leo_fnc_NukeAll;
        _message = format ["%1 objects in %2 has been nuked!", _result, (Leo_PrimaryObjectiveList select _objectiveId) select 0];
    } else {
        _message = "No objective found. You have to move you in a objective to destroy it.";
    };
    
    _args = [_message];    
    ZEN_FMW_MP_REClient("Leo_fnc_Debug", _args, call, _player);
        
    call Zen_StackRemove;
    (0)
};

Leo_fnc_DeleteAll = {
    _Zen_stack_Trace = ["Leo_fnc_DeleteAll", _this] call Zen_StackAdd;
    private ["_objects", "_counter"];

    if !([_this, [["VOID"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _objects = _this select 0;
    _counter = 0;
	{
		deleteVehicle _x;
        _counter = _counter + 1;
	} forEach ([_objects] call Zen_ConvertToObjectArray);
    
    call Zen_StackRemove;
    (_counter)
};

Leo_fnc_RemoveTrack = {
    _Zen_stack_Trace = ["Leo_fnc_RemoveTrack", _this] call Zen_StackAdd;
    private ["_trackArray", "_trackMarkers", "_trackHandle"];

    if !([_this, [["ARRAY"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _trackArray = _this select 0;
    _trackMarkers = _trackArray select 0;
    _trackHandle = _trackArray select 1;
    
    if (!scriptDone _trackHandle) then {
        terminate _trackHandle;
    };
    
    {
        deleteMarker _x;
    } forEach _trackMarkers;
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_KillScript = {
    _Zen_stack_Trace = ["Leo_fnc_KillScript", _this] call Zen_StackAdd;
    private ["_scriptHandle"];

    if !([_this, [["SCRIPT"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _scriptHandle = _this select 0;
    
    if (!scriptDone _scriptHandle) then {
        terminate _scriptHandle;
    };
    
    call Zen_StackRemove;
    (0)
};

Leo_fnc_GetInfantryClasses = {
    private ["_nMenSquad", "_nMenDiverSquad", "_nMenReconSquad", "_nMenSniperSquad", "_infantryClasses"];
    _nMenSquad = _this select 0;
    _nMenDiverSquad = _this select 1;
    _nMenReconSquad = _this select 2;
    _nMenSniperSquad = _this select 3;
    
    _infantryClasses = [];
    
    for "_x" from 1 to _nMenSquad do {
        [_infantryClasses, "Men"] call Zen_ArrayAppend;
    };
    for "_x" from 1 to _nMenDiverSquad do {
        [_infantryClasses, "MenDiver"] call Zen_ArrayAppend;
    };
    for "_x" from 1 to _nMenReconSquad do {
        [_infantryClasses, "MenRecon"] call Zen_ArrayAppend;
    };
    for "_x" from 1 to _nMenSniperSquad do {
        [_infantryClasses, "MenSniper"] call Zen_ArrayAppend;
    };
    
    (_infantryClasses)
};

Leo_fnc_GetVehiclesClasses = {
    private ["_vehiclesType", "_vehiclesClasses"];
    _vehiclesType = _this select 0;
    
    _vehiclesClasses = [];
    
    {
        switch (_x) do {
            case "mrap": {[_vehiclesClasses, Leo_MrapVehiclesClasses] call Zen_ArrayAppendNested};
            case "tank": {[_vehiclesClasses, Leo_TankVehiclesClasses] call Zen_ArrayAppendNested};
            case "acp": {[_vehiclesClasses, Leo_AcpVehiclesClasses] call Zen_ArrayAppendNested};
        };    
    } forEach _vehiclesType;
    
    (_vehiclesClasses)
};

Leo_fnc_GetNearPoint = {
    private ["_position"];
    _position = [_this] call Zen_ConvertToPosition;    
    ([_position, Leo_ExtendedPositionRadius, random 360] call Zen_ExtendPosition)
};

Leo_fnc_InitRandomPatrol = {
    for "_x" from 1 to Leo_nRandomPatrol do {
        0 = [] spawn Leo_fnc_RandomPatrolTask;
    };
};

Leo_fnc_UpdateAlivePlayersTask = {
    while {true} do {
        Leo_AlivePlayers = allPlayers select {alive _x};
        
        sleep Leo_TriggerInterval;
    };
};

Leo_fnc_DeployTent = {
    _Zen_stack_Trace = ["Leo_fnc_DeployTent", _this] call Zen_StackAdd;
    private ["_unit", "_args", "_group", "_groupOwner", "_tent", "_marker", "_oldTent"];
    
    if !([_this, [["OBJECT"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _unit = _this select 0;
    _group = group _unit;
    if ((leader _group) != _unit) exitWith {        
        _args = ["You must be the squad leader to deploy the tent"];
        ZEN_FMW_MP_REClient("Leo_fnc_GroupChat", _args, call, _unit);
                
        call Zen_StackRemove;
        (0)
    };
    
    _groupOwner = groupOwner _group;

    _tent = [_unit, "Land_TentDome_F"] call Zen_SpawnVehicle;
    _tent enableSimulationGlobal false;
    _tent allowDamage false;
    
    _marker = [_unit, groupId _group + " Tent", "colorGreen", [1,1], "hd_dot"] call Zen_SpawnMarker;
    
    if (count Leo_GroupTentList > _groupOwner) then {
        _oldTent = Leo_GroupTentList select _groupOwner;
        if (typeName _oldTent == "ARRAY") then {
            deleteVehicle (_oldTent select 0);
            deleteMarker (_oldTent select 1);
        };
    };     
    Leo_GroupTentList set [_groupOwner, [_tent, _marker]];
    
    _args = ["Now all units of your group can be teleported to the tent"];
    ZEN_FMW_MP_REClient("Leo_fnc_GroupChat", _args, call, _unit);
    
    _groupUnits = units _group;
    [_groupUnits, _unit] call Zen_ArrayRemoveValue;
    _args = ["Your squad leader has just deployed the tent"];
    {        
        ZEN_FMW_MP_REClient("Leo_fnc_GroupChat", _args, call, _x);    
    } forEach _groupUnits;
        
    call Zen_StackRemove;
    (0)
};

Leo_fnc_TeleportPlayer = {
    _Zen_stack_Trace = ["Leo_fnc_DeployTent", _this] call Zen_StackAdd;
    private ["_unit", "_args", "_group", "_groupOwner", "_tent", "_marker", "_oldTent"];
    
    if !([_this, [["OBJECT"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _unit = _this select 0;
    _groupOwner = groupOwner group _unit;
    
    if (count Leo_GroupTentList > _groupOwner) then {
        _oldTent = Leo_GroupTentList select _groupOwner;
        if (typeName _oldTent == "ARRAY") then {
            _args = [getPos (_oldTent select 0)];
            ZEN_FMW_MP_REClient("Leo_fnc_ExecuteTeleport", _args, call, _unit);
        } else {
            _args = ["The tent of your group has not yet been deployed"];
            ZEN_FMW_MP_REClient("Leo_fnc_GroupChat", _args, call, _unit);
        };
    } else {
        _args = ["The tent of your group has not yet been deployed"];
        ZEN_FMW_MP_REClient("Leo_fnc_GroupChat", _args, call, _unit);
    };
    
    call Zen_StackRemove;
    (0)
};


Leo_fnc_CommandUpdateRespawnPoints = {
    _Zen_stack_Trace = ["Leo_fnc_CommandUpdateRespawnPoints", _this] call Zen_StackAdd;
    private ["_players", "_respawnPoints", "_args"];
        
    if !([_this, [["ARRAY"]], [], 1] call Zen_CheckArguments) exitWith {
        call Zen_StackRemove;
        (0)
    };
    
    _players = _this select 0;
    _respawnPoints = [];
    [_respawnPoints, INS_REV_CFG_list_of_respawn_locations_blufor] call Zen_ArrayCopy;
    
    _args = [_respawnPoints];
    
    if (count _players > 0) then {
        {
            ZEN_FMW_MP_REClient("Leo_fnc_UpdateLocalRespawnPoints", _args, call, _x);
        } forEach _players;
    } else {
        ZEN_FMW_MP_RENonDedicated("Leo_fnc_UpdateLocalRespawnPoints", _args, call);
    };
    
    call Zen_StackRemove;
    (0)
};


Leo_fnc_Test = {
    0 = [(Leo_PrimaryObjectivesInfo select 15) select 1] spawn Leo_fnc_NukeAll;
    
    
    //_marker = [24] call Leo_fnc_GetMarker;
    /*
	_aircraft = ([24] call Leo_fnc_AircraftPatrol) select 0;
    0 = [_aircraft, _marker] spawn Zen_OrderAircraftPatrol;
    
    _heli = ([24] call Leo_fnc_HeliPatrol) select 0;
    0 = [_heli, _marker] spawn Zen_OrderAircraftPatrol;
    */
    //_boat = ([24] call Leo_fnc_BoatPatrol) select 0;
    //0 = [_boat, (Leo_PrimaryObjectiveList select 24) select 1, 500] spawn Zen_OrderBoatPatrol;
    
    
    //0 = [24] call Leo_fnc_BoatInsertion;
    //0 = [24, "fastrope"] call Leo_fnc_HeliInsertion;
    
    //0 = [24] call Leo_fnc_ConvoyInsertion;
	
    
    
};
