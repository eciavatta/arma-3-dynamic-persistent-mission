
_Zen_stack_Trace = ["Leo_fnc_CallReinforcements", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveName", "_objectiveSize", "_nReinforcements", "_reinforcementsTasks", "_allDone"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

_objectiveId = _this select 0;
_objectiveName = (Leo_PrimaryObjectiveList select _objectiveId) select 0;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;

_nReinforcements = 0;
switch (_objectiveSize) do {
	case 0: {_nReinforcements = Leo_nReinforcementsSmallTown};
	case 1: {_nReinforcements = Leo_nReinforcementsMediumTown};
    case 2: {_nReinforcements = Leo_nReinforcementsBigTown};
};

_reinforcementsTasks = [];
for "_x" from 1 to _nReinforcements do {
    [_reinforcementsTasks, [_objectiveId] spawn Leo_fnc_ManageReinforcementTask] call Zen_ArrayAppend;
};

[format ["Reinforcements called in city of %1. Number of reinforcements called: %2", _objectiveName, _nReinforcements]] call Leo_fnc_Debug;

waitUntil {
    _allDone = true;    
    {
        _allDone = _allDone and scriptDone _x;    
    } forEach _reinforcementsTasks;
    
    sleep Leo_TriggerInterval;
    (_allDone)
}

call Zen_StackRemove;
(0)
