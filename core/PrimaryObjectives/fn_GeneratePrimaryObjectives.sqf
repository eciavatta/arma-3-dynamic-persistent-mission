
_Zen_stack_Trace = ["Leo_fnc_GetTowns", _this] call Zen_StackAdd;
private ["_PrimaryObjectiveList", "_worldCount", "_loc", "_addThis", "_name", "_position", "_radiusA", "_radiusB", "_size", "_radius", "_area", "_tmpPosObject"];
_PrimaryObjectiveList = [];

_worldCount = count (configFile >> "CfgWorlds" >> worldName >> "Names");

for "_x" from 0 to _worldCount - 1 do {
	_loc = ((configFile >> "CfgWorlds" >> worldName >> "Names") select _x);

	_addThis = false;
	_name = "";
	_position = [0,0,0];
	_radiusA = 0;
	_radiusB = 0;
	for "_y" from 0 to count(_loc) - 1 do {
		switch (configName(_loc select _y)) do {
			case "demography": {
				if (getText(_loc select _y) == "CIV") then {_addThis = true};
			};
			case "name": {_name = getText(_loc select _y)};
			case "position": {_position = getArray(_loc select _y)};
			case "radiusA": {_radiusA = getNumber(_loc select _y)};
			case "radiusB": {_radiusB = getNumber(_loc select _y)};
		};
	};

	if (_addThis && count(_name) > 0) then {
		_size = 0;
		_radius = 0;
		_area = _radiusA * _radiusB;
		if (_area > Leo_SmallTownMaxArea) then {
			if (_area > Leo_MediumTownMaxArea) then {
				_size = 2;
				_radius = Leo_BigTownRadius;
			} else {
				_size = 1;
				_radius = Leo_MediumTownRadius;
			};		
		} else {
			_size = 0;
			_radius = Leo_SmallTownRadius;
		};

		if (worldName != "Altis" || _name != "Gravia") then {
			_tmpPosObject = "RoadCone_F" createVehicle _position;
			_PrimaryObjectiveList pushBack [_name, getPos _tmpPosObject, _size, _radius, false];
			deleteVehicle _tmpPosObject;
		};		
	};	
};

call Zen_StackRemove;
(_PrimaryObjectiveList)
