
_Zen_stack_Trace = ["Leo_fnc_CreateObjective", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveName", "_objectiveSize", "_objectiveMarker", "_infantry", "_vehicles", "_infantryPatrolHandle",
         "_vehiclePatrolHandle", "_infantryTrackArray", "_vehiclesTrackArray", "_civilians", "_explosives", "_ammoBoxes", "_startTime", "_stopTime"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectiveName = (Leo_PrimaryObjectiveList select _objectiveId) select 0;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveMarker = [_objectiveId] call Leo_fnc_GetMarker;

_startTime = diag_tickTime;
[format ["Starting to create primary objective in %1. City size: %2", _objectiveName, _objectiveSize]] call Leo_fnc_Debug;

_infantry = [_objectiveId, false] call Leo_fnc_SpawnInfantry;
_vehicles = [_objectiveId, false] call Leo_fnc_SpawnVehicles;

_infantryPatrolHandle = [_infantry, _objectiveMarker, [], 0, "limited", "aware", true, false, false] spawn Zen_OrderInfantryPatrol;
_vehiclePatrolHandle = [_vehicles select 0, _objectiveMarker, [], 0, "limited", "aware", false, true] spawn Zen_OrderVehiclePatrol;

_infantryTrackArray = [_infantry] call Leo_fnc_TrackEnemies;
_vehiclesTrackArray = [_vehicles select 0] call Leo_fnc_TrackVehicles;

_civilians = [_objectiveId] call Leo_fnc_SpawnCivilian;
_explosives = [_objectiveId] call Leo_fnc_SpawnMines;
_ammoBoxes = [_objectiveId] call Leo_fnc_SpawnAmmoBoxes;

_stopTime = diag_tickTime;
[format ["Ending to create primary objective in %1 in %2 seconds", _objectiveName, _stopTime - _startTime]] call Leo_fnc_Debug;
[format ["Spawned objects: %1 infantry groups, %2 vehicles, %3 vehicle crew, %4 civilians, %5 civilian vehicles, %6 explosives, %7 ammo boxes",
    count _infantry, count (_vehicles select 0), count (_vehicles select 1), count (units (_civilians select 0)), count (_civilians select 1),
    count _explosives, count _ammoBoxes]] call Leo_fnc_Debug;

call Zen_StackRemove;
([[_infantry, _vehicles, _civilians, _explosives, _ammoBoxes], [_infantryPatrolHandle, _vehiclePatrolHandle], [_infantryTrackArray, _vehiclesTrackArray]])
