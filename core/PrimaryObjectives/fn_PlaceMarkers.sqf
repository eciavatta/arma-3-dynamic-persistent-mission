
_Zen_stack_Trace = ["Leo_fnc_PlaceMarkers", _this] call Zen_StackAdd;
private ["_TownList", "_poList"];
_TownList = _this select 0;
_poList = _this select 1;

for "_x" from 0 to count _TownList - 1 do {
	_town = _TownList select _x;
	[_town, _x] call Leo_fnc_CreateMarker;
	[_x, (_poList select _x) select 0] call Leo_fnc_SetMarkerSide;
};

call Zen_StackRemove;
(0)
