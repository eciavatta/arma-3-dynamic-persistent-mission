
_Zen_stack_Trace = ["Leo_fnc_DestroyObjective", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveName", "_spawnedObjects", "_cacheId", "_patrolHandles", "_trackArrays"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

_objectiveId = _this select 0;
_objectiveName = (Leo_PrimaryObjectiveList select _objectiveId) select 0;

_spawnedObjects = (Leo_PrimaryObjectivesInfo select _objectiveId) select 1;
_cacheId = (Leo_PrimaryObjectivesInfo select _objectiveId) select 2;
_patrolHandles = (Leo_PrimaryObjectivesInfo select _objectiveId) select 3;
_trackArrays = (Leo_PrimaryObjectivesInfo select _objectiveId) select 4;

[_trackArrays select 0] call Leo_fnc_RemoveTrack;
[_trackArrays select 1] call Leo_fnc_RemoveTrack;
[_patrolHandles select 0] call Leo_fnc_KillScript;
[_patrolHandles select 1] call Leo_fnc_KillScript;
[_cacheId] call Zen_RemoveCache;
[_spawnedObjects] call Leo_fnc_DeleteAll;

(Leo_PrimaryObjectivesInfo select _objectiveId) set [0, scriptNull];        
(Leo_PrimaryObjectivesInfo select _objectiveId) set [1, []];
(Leo_PrimaryObjectivesInfo select _objectiveId) set [2, ""];
(Leo_PrimaryObjectivesInfo select _objectiveId) set [3, []];
(Leo_PrimaryObjectivesInfo select _objectiveId) set [4, []];

[format ["Primary objective in %1 destroyed.", _objectiveName]] call Leo_fnc_Debug;

call Zen_StackRemove;
(0)
