
_Zen_stack_Trace = ["Leo_fnc_SaveObjectiveToCache", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveName", "_spawnedObjects", "_cacheId", "_patrolHandles", "_trackArrays",
         "_allObjects", "_aliveObjects", "_deadObjects", "_aliveCount", "_deadCount"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ("")
};

_objectiveId = _this select 0;
_objectiveName = (Leo_PrimaryObjectiveList select _objectiveId) select 0;

_spawnedObjects = (Leo_PrimaryObjectivesInfo select _objectiveId) select 1;
_cacheId = (Leo_PrimaryObjectivesInfo select _objectiveId) select 2;
_patrolHandles = (Leo_PrimaryObjectivesInfo select _objectiveId) select 3;
_trackArrays = (Leo_PrimaryObjectivesInfo select _objectiveId) select 4;

[_trackArrays select 0] call Leo_fnc_RemoveTrack;
[_trackArrays select 1] call Leo_fnc_RemoveTrack;
[_patrolHandles select 0] call Leo_fnc_KillScript;
[_patrolHandles select 1] call Leo_fnc_KillScript;

_allObjects = [_spawnedObjects] call Zen_ConvertToObjectArray;
_aliveObjects = _allObjects select {!isNull _x && alive _x};
_deadObjects = _allObjects select {isNull _x || !alive _x};
_aliveCount = count _aliveObjects;
_deadCount = count _deadObjects;

[_deadObjects] call Leo_fnc_DeleteAll;

if (count _cacheId > 0) then {
    _cacheId = [_cacheId] call Zen_Cache;
} else {
    _cacheId = [_aliveObjects] call Zen_Cache;
};

copyToClipboard str ([_cacheId] call Zen_GetCachedUnits);

[format ["Primary objective in %1 saved to cache. Alive objects count: %2, dead objects count: %3, total: %4",
    _objectiveName, _aliveCount, _deadCount, _aliveCount + _deadCount]] call Leo_fnc_Debug;

call Zen_StackRemove;
(_cacheId)
