
_Zen_stack_Trace = ["Leo_fnc_SetMarkerSide", _this] call Zen_StackAdd;
private ["_objectiveId", "_isTaken", "_markerBorder", "_markerDiagonal"];

if !([_this, [["SCALAR"], ["BOOL"]], [], 2] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

_objectiveId = _this select 0;
_isTaken = _this select 1;

_markerBorder = format ["PO_MarkerBorder_%1", _objectiveId];
_markerDiagonal = format ["PO_MarkerDiagonal_%1", _objectiveId];

if (_isTaken) then {
	_markerBorder setMarkerColor "colorBLUFOR";
	_markerDiagonal setMarkerColor "colorBLUFOR";
} else {
	_markerBorder setMarkerColor "colorOPFOR";
	_markerDiagonal setMarkerColor "colorOPFOR";
};

call Zen_StackRemove;
(0)
