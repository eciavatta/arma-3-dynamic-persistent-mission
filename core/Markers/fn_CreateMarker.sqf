
_Zen_stack_Trace = ["Leo_fnc_CreateMarker", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePosition", "_objectiveRadius", "_markerSize", "_markerBorder", "_markerDiagonal"];

if !([_this, [["SCALAR"], ["ARRAY"], ["SCALAR"]], [], 3] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ("")
};

_objectiveId = _this select 0;
_objectivePosition = _this select 1;
_objectiveRadius = _this select 2;

_markerSize = [_objectiveRadius, _objectiveRadius];
_markerBorder = createMarker [format ["PO_MarkerBorder_%1", _objectiveId], _objectivePosition];
_markerBorder setMarkerShape "ELLIPSE";
_markerBorder setMarkerSize _markerSize;
_markerBorder setMarkerBrush "BORDER";	

_markerDiagonal = createMarker [format ["PO_MarkerDiagonal_%1", _objectiveId], _objectivePosition];
_markerDiagonal setMarkerShape "ELLIPSE";
_markerDiagonal setMarkerSize _markerSize;
_markerDiagonal setMarkerBrush "FDiagonal";

call Zen_StackRemove;
(_markerBorder)
