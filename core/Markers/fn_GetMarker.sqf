
_Zen_stack_Trace = ["Leo_fnc_GetMarker", _this] call Zen_StackAdd;
private ["_objectiveId", "_markerStr"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ("")
};

_objectiveId = _this select 0;
_markerStr = format ["PO_MarkerBorder_%1", _objectiveId];

call Zen_StackRemove;
(_markerStr)
