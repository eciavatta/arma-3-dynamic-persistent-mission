class Leo
{
	tag = "Leo";
	class Markers
	{
		file = "core\Markers";
		class SetMarkerSide {description = "";};
		class CreateMarker {description = "";};
		class GetMarker {description = "";};
	};
    class Patrol
	{
		file = "core\Patrol";		
		class RandomPatrolTask {description = "";};
	};
	class Positions
	{
		file = "core\Positions";		
		class SomeoneInArea {description = "";};
	};
	class PrimaryObjectives
	{
		file = "core\PrimaryObjectives";
        class CallReinforcements {description = "";};
        class CreateObjective {description = "";};
		class DestroyObjective {description = "";};		
		class GeneratePrimaryObjectives {description = "";};
        class LoadObjectiveFromCache {description = "";};
		class PlaceMarkers {description = "";};
        class SaveObjectiveToCache {description = "";};
	};
    class Reinforcements
	{
		file = "core\Reinforcements";
        class AircraftPatrol {description = "";};
        class BoatInsertion {description = "";};
        class BoatPatrol {description = "";};
        class ConvoyInsertion {description = "";};
        class HeliInsertion {description = "";};
        class HeliPatrol {description = "";};
	};
	class SecondaryMissions
	{
		file = "core\SecondaryMissions";
	};
	class Spawn
	{
		file = "core\Spawn";
		class SpawnAmmoBoxes {description = "";};
        class SpawnCivilian {description = "";};
		class SpawnInfantry {description = "";};
		class SpawnMines {description = "";};
		class SpawnVehicles {description = "";};
	};
	class Tasks
	{
		file = "core\Tasks";
		class ActiveObjectiveTask {description = "";};
		class ConqueredObjectiveTask {description = "";};
		class InactiveObjectiveTask {description = "";};
        class ManageReinforcementTask {description = "";};
	};
	class Teleport
	{
		file = "core\Teleport";
        class InitTeleport {description = "";};
	};
};