
_Zen_stack_Trace = ["Leo_fnc_HeliInsertion", _this] call Zen_StackAdd;
private ["_objectiveId", "_insertionType", "_objectivePos", "_objectiveSize", "_objectiveRadius",
         "_spawnPos", "_spawnDistance", "_distance", "_insertionPos", "_heli", "_freeSeats", "_group"];

if !([_this, [["SCALAR"], ["STRING"]], [], 2] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    (0)
};

_objectiveId = _this select 0;
_insertionType = _this select 1;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;

_spawnPos = [Leo_MarkersWaterSpawn] call Zen_ArrayGetRandom;
_spawnDistance = Leo_BigScalar;
{
    _distance = [_objectivePos, _x] call Zen_Find2dDistance;    
    if (_distance < _spawnDistance) then {
        _spawnPos = _x;
        _spawnDistance = _distance;
    };
} forEach Leo_MarkersWaterSpawn;

_insertionPos = [_objectivePos, [_objectiveRadius, _objectiveRadius + Leo_TraceRadius], 0, 1, 0, 0,
            [1, 0, Leo_ScanHouseRadius], 0, 0, [1, Leo_MaxPlainSlopeRadius, -1], [1, 0, Leo_ScanAmbientClutterRadius]] call Zen_FindGroundPosition;

_heli = [_spawnPos, Leo_TransportHeliClasses] call Zen_SpawnHelicopter;
_freeSeats = [_heli] call Zen_GetFreeSeats;
_group = [[0,0,0], east, "SOF", count (_freeSeats select 2), "Men", "OPF_F"] call Zen_SpawnInfantry;
if ([_insertionType, "parachute"] call Zen_ValuesAreEqual) then {
    {
        removeBackpack _x;
        _x addBackpack "B_Parachute";
    } forEach (units _group);
};

0 = [_group, _heli] spawn Zen_MoveInVehicle;
0 = [_heli, [_insertionPos, _spawnPos], _group, "full", Leo_HeliHeightFly, _insertionType, true] spawn Zen_OrderInsertion;

call Zen_StackRemove;
(_group)
