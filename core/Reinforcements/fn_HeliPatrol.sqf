
_Zen_stack_Trace = ["Leo_fnc_HeliPatrol", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePos", "_objectiveSize", "_objectiveRadius",
         "_spawnMrk", "_spawnDistance", "_distance", "_spawnPos", "_heli", "_group"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;
_groupList = [];

_spawnMrk = [Leo_MarkersWaterSpawn] call Zen_ArrayGetRandom;
_spawnDistance = Leo_BigScalar;
{
    _distance = [_objectivePos, _x] call Zen_Find2dDistance;    
    if (_distance < _spawnDistance) then {
        _spawnMrk = _x;
        _spawnDistance = _distance;
    };
} forEach Leo_MarkersWaterSpawn;

_spawnPos = [_spawnMrk, 0, 0, 2] call Zen_FindGroundPosition;
_heli = [_spawnPos, Leo_AttackHeliClasses, Leo_HeliHeightFly] call Zen_SpawnHelicopter;
_group = group ((crew _heli) select 0);

call Zen_StackRemove;
([_heli, _group])
