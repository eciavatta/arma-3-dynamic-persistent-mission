
_Zen_stack_Trace = ["Leo_fnc_BoatInsertion", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePos", "_objectiveSize", "_objectiveRadius", "_groupList",
         "_spawnPos", "_insertionPos", "_spawnPos", "_boat", "_freeSeats", "_group"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;
_groupList = [];

_spawnDistance = _objectiveRadius + Leo_ExtendedRadius * 2;
_spawnPos = [_objectivePos, [_spawnDistance, _spawnDistance + Leo_TraceRadius], 0, 2] call Zen_FindGroundPosition;
_insertionPos = [_objectivePos, [_objectiveRadius, _objectiveRadius + Leo_TraceRadius], 0, 1, 0, 0, 0, 0, [2, Leo_ScanWaterRadius]] call Zen_FindGroundPosition;

for "_x" from 0 to 1 do {
    _boat = [_spawnPos call Leo_fnc_GetNearPoint, Leo_TransportBoatClasses] call Zen_SpawnBoat;
    _freeSeats = [_boat] call Zen_GetFreeSeats;
    _group = [[0,0,0], east, "SOF", count (_freeSeats select 1), "Men", "OPF_F"] call Zen_SpawnInfantry;
    
    0 = [_groupList, _group] call Zen_ArrayAppend;    
    0 = [_group, _boat] spawn Zen_MoveInVehicle;
    0 = [_boat, [_insertionPos call Leo_fnc_GetNearPoint, _spawnPos call Leo_fnc_GetNearPoint], _group, "full", 0, "land", true] spawn Zen_OrderInsertion;
};

call Zen_StackRemove;
(_groupList)
