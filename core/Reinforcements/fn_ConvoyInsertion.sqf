
_Zen_stack_Trace = ["Leo_fnc_ConvoyInsertion", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePos", "_objectiveSize", "_objectiveRadius", "_group",
         "_spawnDistance", "_spawnPos", "_insertionPos", "_truck", "_freeSeats", "_group"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;

_spawnDistance = _objectiveRadius + Leo_ExtendedRadius * 3;
_spawnPos = [_objectivePos, [_spawnDistance, _spawnDistance + Leo_TraceRadius], 0, 1, [2, Leo_ScanRoadRadius]] call Zen_FindGroundPosition;
_insertionPos = [_objectivePos, [0, _objectiveRadius], 0, 1, [2, Leo_ScanRoadRadius]] call Zen_FindGroundPosition;

_truck = [_spawnPos, Leo_TransportTruckClasses] call Zen_SpawnGroundVehicle;
_freeSeats = [_truck] call Zen_GetFreeSeats;
_nFreeSeats = count (_freeSeats select 2);
if (_nFreeSeats > Leo_MaxSquadComponents) then {
    _nFreeSeats = Leo_MaxSquadComponents;
};
_group = [[0,0,0], east, "SOF", _nFreeSeats, "Men", "OPF_F"] call Zen_SpawnInfantry;
    
0 = [_group, _truck] spawn Zen_MoveInVehicle;
0 = [_truck, [_insertionPos, _spawnPos], _group, "full", 0, "land", true] spawn Zen_OrderInsertion;

call Zen_StackRemove;
(_group)
