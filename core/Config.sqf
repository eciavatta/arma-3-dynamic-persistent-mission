Leo_SmallTownMaxArea = 275 * 275;
Leo_MediumTownMaxArea = 375 * 375;

Leo_SmallTownRadius = 150;
Leo_MediumTownRadius = 300;
Leo_BigTownRadius = 450;

Leo_ExtendedRadius = 750;

/* Ammo Box for primary objectives */
Leo_nAmmoBoxSmallTown = [1, 3];
Leo_nAmmoBoxMediumTown = [3, 5];
Leo_nAmmoBoxBigTown = [5, 7];

/* Mines for primary objectives
   [nIED, nAntiMan, nAntiVehicle]
*/
Leo_nMinesSmallTown = [3, 5, 2];
Leo_nMinesMediumTown = [6, 8, 4];
Leo_nMinesBigTown = [9, 12, 7];

/* Vehicles Groups for primary objectives */
Leo_nVehiclesSmallTown = [2, 3];
Leo_nVehiclesMediumTown = [3, 4];
Leo_nVehiclesBigTown = [4, 6];

Leo_ScanRoadRadius = 50;
Leo_ScanHouseRadius = 50;
Leo_TraceRadius = 50;
Leo_MaxPlainSlopeRadius = 15;
Leo_ScanAmbientClutterRadius = 25;
Leo_ScanWaterRadius = 50;

Leo_MrapVehiclesClasses = ["O_MRAP_02_gmg_F", "O_MRAP_02_F", "O_MRAP_02_hmg_F"];
Leo_TankVehiclesClasses = ["O_MBT_02_cannon_F", "O_APC_Tracked_02_AA_F"];
Leo_AcpVehiclesClasses = ["O_APC_Tracked_02_cannon_F", "O_APC_Wheeled_02_rcws_F"];

/* Vehicles type. Possible options are:
 *   mrap       MRAPs
 *   tank       general armored vehicles
 *   apc        armored vehicles for carrying and supporting infantry
 */

Leo_VehiclesTypeSmallTown = ["mrap", "apc"];
Leo_VehiclesTypeMediumTown = ["mrap", "tank", "acp"];
Leo_VehiclesTypeBigTown = ["mrap", "tank", "acp"];


Leo_nMenSquadSmallTown = 5;
Leo_nMenSquadMediumTown = 9;
Leo_nMenSquadBigTown = 13;

Leo_nMenDiverSquadSmallTown = 0;
Leo_nMenDiverSquadMediumTown = 0;
Leo_nMenDiverSquadBigTown = 0;

Leo_nMenReconSquadSmallTown = 0;
Leo_nMenReconSquadMediumTown = 1;
Leo_nMenReconSquadBigTown = 1;

Leo_nMenSniperSquadSmallTown = 0;
Leo_nMenSniperSquadMediumTown = 1;
Leo_nMenSniperSquadBigTown = 1;

Leo_nUnitsMenSquad = [6, 8];
Leo_nUnitsMenDiverSquad = [4, 6];
Leo_nUnitsMenReconSquad = [4, 6];
Leo_nUnitsMenSniperSquad = 2;

Leo_nCivilianSmallTown = [4, 6];
Leo_nCivilianMediumTown = [8, 12];
Leo_nCivilianBigTown = [12, 18];

Leo_nCivilianVehiclesSmallTown = [3, 4];
Leo_nCivilianVehiclesMediumTown = [5, 6];
Leo_nCivilianVehiclesBigTown = [7, 8];

Leo_PercentageCivilianInVehicles = 50;

Leo_MarkersWaterSpawn = ["mrkWaterTopSpawn", "mrkWaterRightSpawn", "mrkWaterBottomSpawn", "mrkWaterLeftSpawn"];

Leo_TransportHeliClasses = ["O_Heli_Light_02_F", "O_Heli_Light_02_v2_F", "O_Heli_Light_02_unarmed_F"];
Leo_AttackHeliClasses = ["O_Heli_Attack_02_F", "O_Heli_Attack_02_black_F"];
Leo_AttackBoatClasses = ["O_Boat_Armed_01_hmg_F"];
Leo_TransportBoatClasses = ["O_Boat_Transport_01_F"];
Leo_AircraftClasses = ["O_Plane_CAS_02_F"];

Leo_TransportTruckClasses = ["O_Truck_03_transport_F", "O_Truck_03_covered_F", "O_Truck_02_transport_F", "O_Truck_02_covered_F"];

Leo_HeliHeightFly = 60;
Leo_AircraftHeightFly = 250;

Leo_MaxSquadComponents = 8;

Leo_nRandomPatrol = 5;

Leo_nReinforcementsSmallTown = 1;
Leo_nReinforcementsMediumTown = 2;
Leo_nReinforcementsBigTown = 3;

Leo_ReinforcementsTypesSmallTown = ["boat_insertion", "convoy_insertion", "heli_insertion"];
Leo_ReinforcementsTypesMediumTown = ["boat_insertion", "boat_patrol", "convoy_insertion", "heli_insertion", "heli_patrol"];
Leo_ReinforcementsTypesBigTown = ["aircraft_patrol", "boat_insertion", "boat_patrol", "convoy_insertion", "heli_insertion", "heli_patrol"];

/** Special Variables */
Leo_TriggerInterval = 10;
Leo_BigScalar = 9999999;
Leo_ExtendedPositionRadius = 15;

Leo_DisablePrimaryObjectiveAfterSeconds = 30;
Leo_CallReinforcementsAfter = 30;
