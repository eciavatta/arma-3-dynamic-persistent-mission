
if !(isDedicated) then {
    [west, "Conquer all the objectives established by the enemies.", "Free all objectives", "PO_task"] call Zen_InvokeTaskBriefing;
};

enableSaving [false, false];
sleep 5;

Leo_TrackEnemies = ("Leo_ParamTrackEnemies" call BIS_fnc_getParamValue) == 1;
Leo_DebugEnabled = ("Leo_ParamDebugEnabled" call BIS_fnc_getParamValue) == 1;
Leo_ExtendedDebugEnabled = ("Leo_ParamExtendedDebugEnabled" call BIS_fnc_getParamValue) == 1;

#include "LocalFunctions.sqf"
if !(isDedicated) then {
    #include "InitPlayer.sqf"
};

0 = [] spawn Leo_fnc_InitTeleport;

if !(isServer) exitWith {};
#include "Config.sqf"
#include "ServerFunctions.sqf"
#include "EventQueue.sqf"

/** PrimaryObjective == ["Name", [Position], Size, Radius, isTaken]
    PrimaryObjectiveInfo == [CurrentTaskHandle, [Groups, Civilians, Vehicles, Explosives, AmmoBox], CacheId, [InfantryPatrolHandle, VehiclePatrolHandle], [InfantryTrackArray, VehicleTrackArray], ObjectiveStatus]
    ObjectiveStatus == 0 -> Objective is inactive, 1 -> Enemy is in town, 2 -> Enemy is coming to town */	
Leo_PrimaryObjectiveList = [];
Leo_PrimaryObjectivesInfo = [];
Leo_AlivePlayers = [];

0 = [] spawn Leo_fnc_UpdateAlivePlayersTask;

private ["_i", "_CurrentTaskHandle", "_sleepTime"];

// TODO: Load PrimaryObjectives from database
if (count Leo_PrimaryObjectiveList == 0) then {
	Leo_PrimaryObjectiveList = [] call Leo_fnc_GeneratePrimaryObjectives;
};

if (Leo_DebugEnabled) then {
    testid = 15;
    testpos = (Leo_PrimaryObjectiveList select testid) select 1;
    testsize = (Leo_PrimaryObjectiveList select testid) select 2;
    testradius = (Leo_PrimaryObjectiveList select testid) select 3;
};

_sleepTime = Leo_TriggerInterval / count Leo_PrimaryObjectiveList;

_i = 0;
{
	[ _i, _x select 1, _x select 3] call Leo_fnc_CreateMarker;
	[_i, _x select 4] call Leo_fnc_SetMarkerSide;
	
	if !(_x select 4) then {
		_CurrentTaskHandle = [_i] spawn Leo_fnc_InactiveObjectiveTask;
	} else {
		_CurrentTaskHandle = [_i] spawn Leo_fnc_ConqueredObjectiveTask;
	};
	
	_i = _i + 1;	

	Leo_PrimaryObjectivesInfo pushBack [_CurrentTaskHandle, [[], [], [], [], []], "", scriptNull, [[], scriptNull], 0];
	
	sleep _sleepTime;
} forEach Leo_PrimaryObjectiveList;

0 = [] spawn Leo_fnc_InitRandomPatrol;
