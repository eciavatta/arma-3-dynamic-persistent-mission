
_Zen_stack_Trace = ["Leo_fnc_SpawnCivilian", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePos", "_objectiveSize", "_objectiveRadius",
            "_group", "_vehicleList", "_nCivilian", "_nCivilianVehicles", "_spawnPos", "_civiliansClasses"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;

_civilianList = [];
_vehicleList = [];
_nCivilian = 0;
_nCivilianVehicles = 0;
switch (_objectiveSize) do {
	case 0: {
        _nCivilian = ([Leo_nCivilianSmallTown select 0, Leo_nCivilianSmallTown select 1, true] call Zen_FindInRange);
        _nCivilianVehicles = ([Leo_nCivilianVehiclesSmallTown select 0, Leo_nCivilianVehiclesSmallTown select 1, true] call Zen_FindInRange);
    };
	case 1: {
        _nCivilian = ([Leo_nCivilianMediumTown select 0, Leo_nCivilianMediumTown select 1, true] call Zen_FindInRange);
        _nCivilianVehicles = ([Leo_nCivilianVehiclesMediumTown select 0, Leo_nCivilianVehiclesMediumTown select 1, true] call Zen_FindInRange);
    };
	case 2: {
        _nCivilian = ([Leo_nCivilianBigTown select 0, Leo_nCivilianBigTown select 1, true] call Zen_FindInRange);
        _nCivilianVehicles = ([Leo_nCivilianVehiclesBigTown select 0, Leo_nCivilianVehiclesBigTown select 1, true] call Zen_FindInRange);
    };
};

_group = createGroup civilian;
_civiliansClasses = ["men", civilian, "All", "All", "All", "Unarmed", ""] call Zen_ConfigGetVehicleClasses;
{
    _spawnPos = [_objectivePos, [0, _objectiveRadius], 0, 1] call Zen_FindGroundPosition;
    _x createUnit [_spawnPos, _group];
} forEach ([_civiliansClasses, _nCivilian] call Zen_ArrayGetRandomSequence);
doStop (units _group);

_vehicleList = [_objectivePos, _objectiveRadius, _nCivilianVehicles] call Zen_SpawnAmbientVehicles;
{
    if (random 100 >= Leo_PercentageCivilianInVehicles) then {
        ([units _group] call Zen_ArrayGetRandom) moveInDriver _x;
    };
} forEach _vehicleList;

call Zen_StackRemove;
([_group, _vehicleList])
