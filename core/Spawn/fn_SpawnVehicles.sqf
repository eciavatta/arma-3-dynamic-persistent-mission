
_Zen_stack_Trace = ["Leo_fnc_SpawnVehicles", _this] call Zen_StackAdd;
private ["_objectiveId", "_isNewTroop", "_objectivePos", "_objectiveSize", "_objectiveRadius",
         "_vehicleList", "_nVehicles", "_groupList", "_vehiclesClasses", "_spawnPos", "_vehicle", "_group"];

if !([_this, [["SCALAR"], ["BOOL"]], [], 2] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_isNewTroop = _this select 1;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;

_vehicleList = [];
_nVehicles = 0;
_groupList = [];
_vehiclesClasses = [];
switch (_objectiveSize) do {
	case 0: {
        _nVehicles = ([Leo_nVehiclesSmallTown select 0, Leo_nVehiclesSmallTown select 1, true] call Zen_FindInRange);
        _vehiclesClasses = [Leo_VehiclesTypeSmallTown] call Leo_fnc_GetVehiclesClasses;
    };
	case 1: {
        _nVehicles = ([Leo_nVehiclesMediumTown select 0, Leo_nVehiclesMediumTown select 1, true] call Zen_FindInRange);
        _vehiclesClasses = [Leo_VehiclesTypeMediumTown] call Leo_fnc_GetVehiclesClasses;
    };
	case 2: {
        _nVehicles = ([Leo_nVehiclesBigTown select 0, Leo_nVehiclesBigTown select 1, true] call Zen_FindInRange);
        _vehiclesClasses = [Leo_VehiclesTypeBigTown] call Leo_fnc_GetVehiclesClasses;
    };
};

for "_x" from 0 to _nVehicles do {
	if (_isNewTroop) then {
		_spawnPos = [_objectivePos, [_objectiveRadius + Leo_ExtendedRadius, _objectiveRadius + Leo_ExtendedRadius + Leo_TraceRadius],
					 0, 1, [2, Leo_ScanRoadRadius]] call Zen_FindGroundPosition;
	} else {
		_spawnPos = [_objectivePos, [0, _objectiveRadius], 0, 1, [2, Leo_ScanRoadRadius]] call Zen_FindGroundPosition;	
	};	
	_vehicle = [_spawnPos, _vehiclesClasses, [_spawnPos, _objectivePos] call Zen_FindDirection] call Zen_SpawnGroundVehicle;
	0 = [_vehicleList, _vehicle] call Zen_ArrayAppend;
	_group = group ((crew _vehicle) select 0);
	0 = [_groupList, _group] call Zen_ArrayAppend;
};

call Zen_StackRemove;
([_vehicleList, _groupList])
