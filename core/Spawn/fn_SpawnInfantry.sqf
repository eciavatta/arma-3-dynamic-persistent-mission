
_Zen_stack_Trace = ["Leo_fnc_SpawnInfantry", _this] call Zen_StackAdd;
private ["_objectiveId", "_isNewTroop", "_objectivePos", "_objectiveSize", "_objectiveRadius",
            "_groupList", "_infantryClasses", "_spawnPos", "_aiSkillPreset", "_nUnits", "_group"];

if !([_this, [["SCALAR"], ["BOOL"]], [], 2] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_isNewTroop = _this select 1;
_objectivePos = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;

_groupList = [];
_infantryClasses = [];
switch (_objectiveSize) do {
	case 0: {_infantryClasses = [Leo_nMenSquadSmallTown, Leo_nMenDiverSquadSmallTown,
                                 Leo_nMenReconSquadSmallTown, Leo_nMenSniperSquadSmallTown] call Leo_fnc_GetInfantryClasses};
	case 1: {_infantryClasses = [Leo_nMenSquadMediumTown, Leo_nMenDiverSquadMediumTown,
                                 Leo_nMenReconSquadMediumTown, Leo_nMenSniperSquadMediumTown] call Leo_fnc_GetInfantryClasses};
	case 2: {_infantryClasses = [Leo_nMenSquadBigTown, Leo_nMenDiverSquadBigTown,
                                 Leo_nMenReconSquadBigTown, Leo_nMenSniperSquadBigTown] call Leo_fnc_GetInfantryClasses};
};

{
    if (_isNewTroop) then {
        _spawnPos = [_objectivePos, [_objectiveRadius + Leo_ExtendedRadius, _objectiveRadius + Leo_ExtendedRadius + Leo_TraceRadius],
					 0, 1] call Zen_FindGroundPosition;
    } else {
        _spawnPos = [_objectivePos, [0, _objectiveRadius], 0, 1] call Zen_FindGroundPosition;
    };
    
    _aiSkillPreset = "Infantry";
    switch (_x) do {
        case "Men": {_aiSkillPreset = "Infantry"};
        case "MenDiver": {_aiSkillPreset = "Crew"};
        case "MenRecon": {_aiSkillPreset = "SOF"};
        case "MenSniper": {_aiSkillPreset = "Sniper"};
    };
    
    _nUnits = Leo_nUnitsMenSquad;
    switch (_x) do {
        case "Men": {_nUnits = Leo_nUnitsMenSquad};
        case "MenDiver": {_nUnits = Leo_nUnitsMenDiverSquad};
        case "MenRecon": {_nUnits = Leo_nUnitsMenReconSquad};
        case "MenSniper": {_nUnits = Leo_nUnitsMenSniperSquad};
    };
    
	_group = [_spawnPos, east, _aiSkillPreset, _nUnits, _x, "OPF_F"] call Zen_SpawnInfantry;
	0 = [_groupList, _group] call Zen_ArrayAppend;
} forEach _infantryClasses;

//0 = [_groupList] call Zen_GiveLoadoutOpfor;
//0 = [_groupList] call Zen_AddGiveMagazine;
//0 = [_groupList] call Zen_AddRepackMagazines;

call Zen_StackRemove;
(_groupList)
