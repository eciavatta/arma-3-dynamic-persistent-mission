
_Zen_stack_Trace = ["Leo_fnc_SpawnAmmoBoxes", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectiveMarker", "_objectiveSize", "_ammoBoxList", "_nAmmoBox", "_centerPos",
         "_buildingName", "_buildingPositions", "_ammoBox", "_containsExplosives"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectiveMarker = [_objectiveId] call Leo_fnc_GetMarker;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;

_ammoBoxList = [];
_nAmmoBox = 0;
_containsExplosives = true;
switch (_objectiveSize) do {
	case 0: {_nAmmoBox = ([Leo_nAmmoBoxSmallTown select 0, Leo_nAmmoBoxSmallTown select 1, true] call Zen_FindInRange)};
	case 1: {_nAmmoBox = ([Leo_nAmmoBoxMediumTown select 0, Leo_nAmmoBoxMediumTown select 1, true] call Zen_FindInRange)};
	case 2: {_nAmmoBox = ([Leo_nAmmoBoxBigTown select 0, Leo_nAmmoBoxBigTown select 1, true] call Zen_FindInRange)};
};

for "_x" from 0 to _nAmmoBox do {
	_centerPos = [_objectiveMarker] call Zen_FindGroundPosition;
	_buildingName = nearestBuilding _centerPos;
	//_buildingPositions = [_centerPos] call Zen_FindBuildingPositions;

	//if (count _buildingPositions == 0) then {
		_buildingPositions = [getPosATL _buildingName];		
	//};
	
	_ammoBox = [[_buildingPositions] call Zen_ArrayGetRandom, east, _containsExplosives] call Zen_SpawnAmmoBox;
	_containsExplosives = !_containsExplosives;
	0 = [_ammoBoxList, _ammoBox] call Zen_ArrayAppend;
};

call Zen_StackRemove;
(_ammoBoxList)
