
_Zen_stack_Trace = ["Leo_fnc_SpawnMines", _this] call Zen_StackAdd;
private ["_objectiveId", "_objectivePosition", "_objectiveSize", "_objectiveRadius", "_mineList", "_nMineArray",
         "_mineNames", "_spawnPos", "_mine"];

if !([_this, [["SCALAR"]], [], 1] call Zen_CheckArguments) exitWith {
    call Zen_StackRemove;
    ([])
};

_objectiveId = _this select 0;
_objectivePosition = (Leo_PrimaryObjectiveList select _objectiveId) select 1;
_objectiveSize = (Leo_PrimaryObjectiveList select _objectiveId) select 2;
_objectiveRadius = (Leo_PrimaryObjectiveList select _objectiveId) select 3;

_mineList = [];
_nMineArray = [0, 0, 0]; // [nIED, nAntiMan, nAntiVehicle]
switch (_objectiveSize) do {
	case 0: {_nMineArray = Leo_nMinesSmallTown};
	case 1: {_nMineArray = Leo_nMinesMediumTown};
	case 2: {_nMineArray = Leo_nMinesBigTown};
};
_mineNames = [["IEDLandBig_F", "IEDUrbanBig_F", "IEDLandSmall_F", "IEDUrbanSmall_F"],
              ["APERSMine", "APERSTripMine", "APERSTripMine"],
			  ["ATMine", "SLAMDirectionalMine"]];

for "_x" from 0 to 2 do {
	for "_y" from 1 to (_nMineArray select _x) do {
		switch (_x) do {
			case 0: {_spawnPos = [_objectivePosition, [0, _objectiveRadius], 0, 1, [1, Leo_ScanRoadRadius], 0,
			                      [2, 3, Leo_ScanHouseRadius]] call Zen_FindGroundPosition};
			case 1: {_spawnPos = [_objectivePosition, [_objectiveRadius, _objectiveRadius + Leo_ExtendedRadius],
			                      0, 1, [3, Leo_ScanRoadRadius], 0, [1, 3, Leo_ScanHouseRadius]] call Zen_FindGroundPosition};
			case 2: {_spawnPos = [_objectivePosition, [_objectiveRadius, _objectiveRadius + Leo_ExtendedRadius],
			                      0, 1, [2, Leo_ScanRoadRadius], 0, [1, 3, Leo_ScanHouseRadius]] call Zen_FindGroundPosition};
		};		
		_mine = [_spawnPos, [_mineNames select _x] call Zen_ArrayGetRandom] call Zen_SpawnVehicle;
		0 = [_mineList, _mine] call Zen_ArrayAppend;
	};
};

call Zen_StackRemove;
(_mineList)
