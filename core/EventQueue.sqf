
Event_Arguments_Array = [];
Event_Queue_Array = [];

f_HandleEventQueue = {
    while {true} do {
        sleep 5;
        {
            if (_x select 1) then {
                _eventName = (_x select 0);
                _qualifier = [_eventName, "@$"] call Zen_StringGetDelimitedPart;
                _functionName = _eventName;
                if (count toArray _qualifier > 0) then {
                    _functionName = [_eventName, "@" + _qualifier + "$", ""] call Zen_StringFindReplace;
                };

                _argsIndex = [Event_Arguments_Array, _eventName, 0] call Zen_ArrayGetNestedIndex;
                _args = (Event_Arguments_Array select _argsIndex) select 1;
                0 = _args spawn (missionNamespace getVariable _functionName);
                0 = [Event_Arguments_Array, _argsIndex] call Zen_ArrayRemoveIndex;
                Event_Queue_Array set [_forEachIndex, 0];
            };
        } forEach Event_Queue_Array;
        0 = [Event_Queue_Array, 0] call Zen_ArrayRemoveValue;
    };
};

/** Usage: "EventName" call f_EventTrigger_Generic;, returns void. */
f_EventTrigger_Generic = {
    _nestedArray = [Event_Queue_Array, _this, 0] call Zen_ArrayGetNestedValue;
    if (count _nestedArray > 0) then {
        _nestedArray set [1, true];
    };
};

f_RemoveEvent = {
    {
        _index = [_x, _this, 0] call Zen_ArrayGetNestedIndex;
        if (_index != -1) then {
            0 = [_x, _index] call Zen_ArrayRemoveIndex;
        };
    } forEach [Event_Arguments_Array, Event_Queue_Array];
};

/** Usage: "EventName" call f_EventIsQueued;, returns boolean. */
f_EventIsQueued = {
    (([Event_Queue_Array, _this, 0] call Zen_ArrayGetNestedIndex) > -1)
};

// Event handling thread
0 = [] spawn f_HandleEventQueue;
