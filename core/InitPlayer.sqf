
//playerLoaded = false;
Leo_DisableDamage = false;
Leo_IsPlayerInBase = true;

// Debug in-game options
if (Leo_ExtendedDebugEnabled) then {
	[player] spawn Leo_fnc_InitExtendedDebug;
	player addEventHandler ["Respawn", {0 = [player] spawn Leo_fnc_InitExtendedDebug}];
};

if ("Leo_AllowFatigue" call BIS_fnc_getParamValue == 0) then {
   player enableFatigue false;
   player addEventhandler ["Respawn", {player enableFatigue false}];
};

0 = [] spawn {
    private ["_allowDamage", "_check", "_args"];
    _allowDamage = true;
    
    while {true} do {
        Leo_IsPlayerInBase = [player, "mrkBase"] call Zen_AreInArea;
        _check = Leo_DisableDamage or Leo_IsPlayerInBase;
        
        if ([_check, _allowDamage] call Zen_ValuesAreEqual) then {
            _allowDamage = !_allowDamage;
            _args = [player, _allowDamage];
            ZEN_FMW_MP_REServerOnly("Leo_fnc_AllowDamageServer", _args, call)
            player allowDamage _allowDamage;
            
            if (Leo_IsPlayerInBase) then {
                player addEventHandler ["Fired", {player commandChat "Hey! Do not shoot into the base."}];
            } else {
                player removeAllEventHandlers "Fired";
            };
        };
        
        sleep 10;
    };
};

//waitUntil { playerLoaded };

// Messaggio di benvenuto
[parseText format [ "<t align='right' size='1.2'><t font='PuristaBold' size='1.6'>%1</t><br />
%2</t>", toUpper "Welcome, soldier!", str(date select 1) + "." + str(date select 2) + "." + str(date select 0)], true, nil, 7, 0.7, 0] spawn BIS_fnc_textTiles;

sleep 5;

// Avviso missione in sviluppo
titleText [format ["co32_dynamic-persistent-mission << Version: DEV [%1] >> by Leotichidas", preprocessFile "version"], "PLAIN"];

[parseText format [ "<t align='right' size='1.2'><t font='PuristaBold' size='1.6'>%1</t><br />
%2</t>", toUpper "Dynamic Persistent Mission", "Written by Leotichidas"], true, nil, 7, 0.7, 0] spawn BIS_fnc_textTiles;
