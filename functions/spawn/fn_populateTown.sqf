
// Arguments
params ["_town"];

// Private variables
private ["_group", "_vehicleList", "_unitList"];

_group = createGroup civilian;
_vehicleList = [];
_unitList = [];

_civilClasses = ["C_man_shorts_1_F","C_man_1_1_F","C_man_1_2_F","C_man_1_3_F","C_man_polo_1_F","C_man_polo_2_F","C_man_polo_3_F","C_man_polo_4_F","C_man_polo_5_F","C_man_polo_6_F"];
_civilVehicles = ["C_Van_01_transport_F", "C_Van_01_fuel_F", "C_Van_01_box_F", "C_Offroad_01_F", "C_Offroad_01_repair_F", "C_Hatchback_01_F", "C_Hatchback_01_sport_F", "C_Quadbike_01_F", "C_SUV_01_F", "C_Truck_02_transport_F", "C_Truck_02_covered_F", "C_Truck_02_fuel_F", "C_Truck_02_box_F"];

_civilNumber = 0;
switch (_town select 2) do {
	case 0: {_civilNumber = 9;};
	case 1: {_civilNumber = 15;};
	case 2: {_civilNumber = 21;};
};

_townPos = _town select 1;
_townRadius = _town select 3;

for "_x" from 1 to _civilNumber do {
	_civilPos = _townPos getPos [random _townRadius, random 360];
	while { surfaceIsWater _civilPos } do {
		_civilPos = _townPos getPos [random _townRadius, random 360];
	};
	
	_unit = _group createUnit [selectRandom _civilClasses, _civilPos, [], random 360, "FORM"];
	doStop _unit;
	_unitList pushBack _unit;

	if ((random 100) < 10) then {
		_vehType = selectRandom _civilVehicles;
		_vehPos = _civilPos findEmptyPosition [10, 50, _vehType];
		_veh = _vehType createVehicle (getPos _unit);
		_unit moveInDriver _veh;
		_vehicleList pushBack _veh;		
	};
};

[_group, _vehicleList, _unitList];
