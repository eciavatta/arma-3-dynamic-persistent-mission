
// Arguments
params ["_groupType", "_position"];

// Private variables
private ["_vehicle", "_group", "_unitList"];

_group = createGroup east;
_vehicle = objNull;
_unitList = [];

if (_groupType in ["mtbac", "mtbaa", "acp", "mraphgm", "mrap", "truck"]) then {

	// Cerca la strada più vicina - i veicoli spawnano su strada
	_searchRadius = 10;
	_roads = _position nearRoads _searchRadius;

	while {count _roads == 0} do {
		_searchRadius = _searchRadius + 10;
		_roads = _position nearRoads _searchRadius;
	};

	_position = getPos (_roads select 0);
};

if (_groupType == "mtbac") then {
	_vehicle = "O_MBT_02_cannon_F" createVehicle _position;

	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_crew_F", "O_crew_F", "O_crew_F", "O_soldier_repair_F", "O_soldier_AA_F", "O_support_GMG_F"];

	_group selectLeader (units _group select 0);	
	(units _group select 0) moveInCommander _vehicle;
	(units _group select 1) moveInDriver _vehicle;
	(units _group select 2) moveInGunner _vehicle;
};

if (_groupType == "mtbaa") then {
	_vehicle = "O_APC_Tracked_02_AA_F" createVehicle _position;

	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_crew_F", "O_crew_F", "O_crew_F", "O_soldier_AT_F", "O_soldier_exp_F", "O_Soldier_GL_F"];

	_group selectLeader (units _group select 0);	
	(units _group select 0) moveInCommander _vehicle;
	(units _group select 1) moveInDriver _vehicle;
	(units _group select 2) moveInGunner _vehicle;
};

if (_groupType == "acp") then {
	_vehicle = "O_MBT_02_arty_F" createVehicle _position;

	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_crew_F", "O_crew_F", "O_crew_F", "O_soldier_AT_F", "O_soldier_repair_F", "O_Soldier_F"];

	_group selectLeader (units _group select 0);	
	(units _group select 0) moveInCommander _vehicle;
	(units _group select 1) moveInDriver _vehicle;
	(units _group select 2) moveInGunner _vehicle;
};

if (_groupType == "mraphgm") then {
	_vehicle = "O_MRAP_02_hmg_F" createVehicle _position;

	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_crew_F", "O_crew_F", "O_Soldier_AA_F", "O_Soldier_TL_F", "O_soldier_M_F", "O_medic_F"];

	_group selectLeader (units _group select 3);
	(units _group select 0) moveInDriver _vehicle;
	(units _group select 1) moveInGunner _vehicle;
};

if (_groupType == "mrap") then {
	_vehicle = "O_MRAP_02_F" createVehicle _position;

	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_crew_F", "O_Soldier_TL_F", "O_Soldier_AT_F", "O_medic_F", "O_Soldier_GL_F", "O_engineer_F"];

	_group selectLeader (units _group select 1);
	(units _group select 0) moveInDriver _vehicle;
};

if (_groupType == "truck") then {
	_vehicle = "O_Truck_03_transport_F" createVehicle _position;

	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_crew_F", "O_Soldier_TL_F", "O_Soldier_F", "O_support_GMG_F", "O_Soldier_GL_F", "O_medic_F"];

	_group selectLeader (units _group select 1);
	(units _group select 0) moveInDriver _vehicle;
	(units _group select 1) moveInCargo _vehicle;
	(units _group select 2) moveInCargo _vehicle;
	(units _group select 3) moveInCargo _vehicle;
	(units _group select 4) moveInCargo _vehicle;
	(units _group select 5) moveInCargo _vehicle;
};

if (_groupType == "soldac") then {
	{
		_unitList pushBack (_group createUnit [_x, _position, [], 0, "FORM"]);
	} forEach ["O_Soldier_AT_F", "O_Soldier_AAT_F", "O_soldier_exp_F", "O_Soldier_TL_F", "O_support_GMG_F", "O_Soldier_A_F"];

	_group selectLeader (units _group select 0);
};

[_group, _vehicle, _unitList];
