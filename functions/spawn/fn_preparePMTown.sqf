
// Arguments
params ["_town"];

_townPosition = _town select 1;
_townSize = _town select 2;
_townRadius = _town select 3;

private ["_groupList", "_vehicleList", "_unitList"];

_groupList = [];
_vehicleList = [];
_unitList = [];

if (_townSize == 0) then {
	{
		_rtn = [_x, _townPosition getPos [random _townRadius, random 360]] call LEO_fnc_spawnEnemyGroup;		
		_groupList pushBack (_rtn select 0);
		if (!isnull (_rtn select 1)) then { _vehicleList pushBack (_rtn select 1); };
		_unitList append (_rtn select 2);		
	} forEach ["mraphgm", "mrap", "soldac", "soldac", "soldac"];
};

if (_townSize == 1) then {
	{
		_rtn = [_x, _townPosition getPos [random _townRadius, random 360]] call LEO_fnc_spawnEnemyGroup;
		_groupList pushBack (_rtn select 0);
		if (!isnull (_rtn select 1)) then { _vehicleList pushBack (_rtn select 1); };
		_unitList append (_rtn select 2);	
	} forEach ["acp", "mraphgm", "soldac", "soldac", "soldac", "soldac", "soldac"];
};

if (_townSize == 2) then {
	{
		_rtn = [_x, _townPosition getPos [random _townRadius, random 360]] call LEO_fnc_spawnEnemyGroup;
		_groupList pushBack (_rtn select 0);
		if (!isnull (_rtn select 1)) then { _vehicleList pushBack (_rtn select 1); };
		_unitList append (_rtn select 2);	
	} forEach ["mtbac", "mtbaa", "acp", "soldac", "soldac", "soldac", "soldac", "soldac", "soldac", "soldac"];
};

[[_groupList, _vehicleList, _unitList], [_town] call LEO_fnc_populateTown, [_town] call LEO_fnc_generateMines];
