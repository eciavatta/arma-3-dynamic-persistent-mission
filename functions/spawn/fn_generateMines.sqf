
// Arguments
params ["_town"];

// Private variables
private ["_mineList", "_iedObjects", "_amObjects", "_acObjects"];

_mineList = [];
_iedObjects = ["IEDLandBig_F", "IEDUrbanBig_F", "IEDLandSmall_F", "IEDUrbanSmall_F"];
_amObjects = ["APERSMine", "APERSTripMine", "APERSTripMine"];
_acObjects = ["ATMine", "SLAMDirectionalMine"];

_minesNumber = 0;
switch (_town select 2) do {
	case 0: {_minesNumber = 9;};
	case 1: {_minesNumber = 15;};
	case 2: {_minesNumber = 21;};
};

_townPos = _town select 1;
_townRadius = _town select 3;

for "_x" from 1 to _minesNumber do {

	_pos = _townPos getPos [random _townRadius, random 360];	
	_mineList pushBack ((selectRandom _iedObjects) createVehicle _pos);


	_pos = _townPos getPos [random (_townRadius * 2), random 360];
	// Cerca la strada più vicina
	_searchRadius = 10;
	_roads = _pos nearRoads _searchRadius;

	while {count _roads == 0} do {
		_searchRadius = _searchRadius + 10;
		_roads = _pos nearRoads _searchRadius;
	};
	_mineList pushBack ((selectRandom _amObjects) createVehicle _pos);


	_pos = _townPos getPos [random (_townRadius * 2), random 360];
	// Cerca la strada più vicina
	_searchRadius = 10;
	_roads = _pos nearRoads _searchRadius;

	while {count _roads == 0} do {
		_searchRadius = _searchRadius + 10;
		_roads = _pos nearRoads _searchRadius;
	};
	_mineList pushBack ((selectRandom _acObjects) createVehicle _pos);
};

_mineList;
