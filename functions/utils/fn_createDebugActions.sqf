
params ["_player"];

_player addAction [
		"<t color='#FFFF00'>Disable damage (Debug)</t>",	/* Title */
		"LEO_VAR_ALLOWDAMAGEONDEBUG = 0",		/* Script */
		[],		/* Arguments */
		0,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"LEO_VAR_ALLOWDAMAGEONDEBUG == 1"		/* Condition */
	];

_player addAction [
		"<t color='#FFFF00'>Enable damage (Debug)</t>",	/* Title */
		"LEO_VAR_ALLOWDAMAGEONDEBUG = 1",		/* Script */
		[],		/* Arguments */
		0,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"LEO_VAR_ALLOWDAMAGEONDEBUG == 0"		/* Condition */
	];

_player addAction [
		"<t color='#FFFF00'>Ammo Box (Debug)</t>",	/* Title */
		"['Open',true] spawn BIS_fnc_arsenal",		/* Script */
		[],		/* Arguments */
		0,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];

true
