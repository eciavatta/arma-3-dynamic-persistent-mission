if (!isserver) exitwith {};

private ["_player", "_status"];
_player = _this select 0;
_status = _this select 1;

if (_status) then {
	_player allowDamage false;
	_player addEventHandler ["fired", {(_this select 0) setvehicleammo 1;}];
	_player enableFatigue false;

	["God mode enabled"] spawn LEO_fnc_log;
} else {
	_player allowDamage true;
	_player removeEventHandler ["fired", 0];
	_player enableFatigue true;

	["God mode disabled"] spawn LEO_fnc_log;
};
