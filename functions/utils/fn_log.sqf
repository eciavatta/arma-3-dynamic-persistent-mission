if (!isServer) exitWith {};

// Arguments
params ["_object", ["_global", false]];

_debug = "LEO_PARAM_DEBUG" call BIS_fnc_getParamValue;
if (_debug == 1) then {
	diag_log _object; // Log object to RTF file

	if (_global) then {
		if (!isNull player) then { hint format ["Debug: %1", _object] }; // Debug when player is the server
		LEO_VAR_debugMessage = format ["Debug: %1", _object];
		publicVariable "LEO_VAR_DEBUGMESSAGE"; // Send message to all clients
	};	
};
