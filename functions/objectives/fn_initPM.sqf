
// Global variables
pmTownList = [];
pmMarkerList = [];
pmTriggerList = [];

pmCurrentIndex = -1; // Nessuna missione attiva
pmCurrentMarkerCreated = false;
pmCurrentMarker = nil; // Marker corrente;

pmEnemyList = [[],[],[]];
pmCivilList = [grpNull,[],[]];
pmMineList = [];

[] call LEO_fnc_retTownList; // trovo tutte le citta' civili

[] call LEO_fnc_genPMMarkers; // Genero gli obbiettivi principali in mappa

[] call LEO_fnc_createPMTriggers; // Creo i trigger per gli obbiettivi principali

[[
  ["pmMainTask","Conquer all enemy cities","Free the island from enemy occupation"]
],[
  ["Note1","Test"]
]] execvm "shk_taskmaster.sqf";
