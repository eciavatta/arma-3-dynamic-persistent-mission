
{
	_size = [_x select 3, _x select 3];	

	_markerBorder = createMarker [format ["pmMarkerBorder%1", _forEachIndex], _x select 1];
	_markerBorder setMarkerShape "ELLIPSE";
	_markerBorder setMarkerSize _size;
	_markerBorder setMarkerBrush "BORDER";
	_markerBorder setMarkerColor "colorOPFOR";

	_markerDiagonal = createMarker [format ["pmMarkerDiagonal%1", _forEachIndex], _x select 1];
	_markerDiagonal setMarkerShape "ELLIPSE";
	_markerDiagonal setMarkerSize _size;
	_markerDiagonal setMarkerBrush "FDiagonal";
	_markerDiagonal setMarkerColor "colorOPFOR";

	pmMarkerList pushBack [_markerBorder, _markerDiagonal];

} forEach pmTownList;

true
