
// Rimuovo veicoli nemici rimasti non occupati da WEST
{
	if (!isNull _x) then {
		if (side _x != west) then {
			{ moveOut _x } forEach crew _x;
			deleteVehicle _x;
		}		
	};
} forEach (pmEnemyList select 1);

// Rimuovo unita' nemiche rimaste
{
	if (!isNull _x) then {
		deleteVehicle _x;
	};
} forEach (pmEnemyList select 2);

// Rimuovo gruppi nemici
{
	if (!isNull _x) then {
		deleteGroup _x;
	};
} forEach (pmEnemyList select 0);


// Rimuovo veicoli civili rimasti non occupati da WEST
{
	if (!isNull _x) then {
		if (side _x != west) then {
			{ moveOut _x } forEach crew _x;
			deleteVehicle _x;
		}		
	};
} forEach (pmCivilList select 1);

// Rimuovo unita' civili rimaste
{
	if (!isNull _x) then {
		deleteVehicle _x;
	};
} forEach (pmCivilList select 2);

// Rimuovo gruppo dei civili
if (!isNull (pmCivilList select 0)) then {
	deleteGroup (pmCivilList select 0);
};


// Rimuovo le mine
{
	if (!isNull _x) then { deleteVehicle _x };
} forEach pmMineList;


// Spawno i nemici e i civili nella nuova citta'
_rtn = [pmTownList select pmCurrentIndex] call LEO_fnc_preparePMTown;
pmEnemyList = _rtn select 0;
pmCivilList = _rtn select 1;
pmMineList = _rtn select 2;
