
// Local variables
private ["_name", "_position", "_size"];

_worldCount = count (configFile >> "CfgWorlds" >> worldName >> "Names");

for "_x" from 0 to _worldCount - 1 do {
	_loc = ((configFile >> "CfgWorlds" >> worldName >> "Names") select _x);

	_addThis = false;
	_radiusA = 0;
	_radiusB = 0;
	for "_y" from 0 to count(_loc) - 1 do {
		switch (configName(_loc select _y)) do {
			case "demography": {
				if (getText(_loc select _y) == "CIV") then {_addThis = true};
			};
			case "name": {_name = getText(_loc select _y)};
			case "position": {_position = getArray(_loc select _y)};
			case "radiusA": {_radiusA = getNumber(_loc select _y)};
			case "radiusB": {_radiusB = getNumber(_loc select _y)};
		};
	};

	if (_addThis && count(_name) > 0) then {
		_size = 0;
		_radius = 0;

		_area = _radiusA * _radiusB;

		if (_area > 275 * 275) then {
			if (_area > 375 * 375) then {
				_size = 2;
				_radius = 450;
			} else {
				_size = 1;
				_radius = 300;
			};		
		} else {
			_size = 0;
			_radius = 150;
		};

		// Non inserisco la città più vicina all'aeroporto su Altis
		if (worldName != "Altis" || _name != "Gravia") then {
			_tmpPosObject = "RoadCone_F" createVehicle _position;
			pmTownList pushBack [_name, getPos _tmpPosObject, _size, _radius];
			deleteVehicle _tmpPosObject;
		};		
	};	
};

true
