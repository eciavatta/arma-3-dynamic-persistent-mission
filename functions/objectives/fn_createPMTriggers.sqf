#include "..\..\includes\config.hpp"

{
	_position = _x select 1;

	_trg = createTrigger ["EmptyDetector", _position];
	_trg setVariable ["trgIndex", _forEachIndex]; // Lo utilizzo quando viene attivato
	_trg setVariable ["alliedPresence", 0]; // Indica se sono presenti unita' all'interno del trigger
	_trg setTriggerArea [PMTRIGGER_RADIUS, PMTRIGGER_RADIUS, 0, false];
	_trg setTriggerActivation [ALLIED_FACTION, "PRESENT", true];
	_trg setTriggerStatements ["this", "[thisTrigger] call LEO_fnc_actPMTriggers", "[thisTrigger] call LEO_fnc_deactPMTriggers"];

	pmTriggerList pushBack _trg;
} forEach pmTownList;

true
