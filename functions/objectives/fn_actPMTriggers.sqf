
params ["_trg"];

_trg setVariable ["alliedPresence", 1];
_trgIndex = _trg getVariable "trgIndex";
_curTown = pmTownList select _trgIndex;

_check = true;

if (pmCurrentIndex == _trgIndex) then {
	[format ["mission already started %1", _curTown select 0], true] spawn LEO_fnc_log;
	_check = false;
};

if (pmCurrentIndex >= 0) then {
	_activeTrg = pmTriggerList select pmCurrentIndex;

	if ((_activeTrg getVariable "alliedPresence") == 1) then {
		[format ["skipping primary mission in %1", _curTown select 0], true] spawn LEO_fnc_log;
		_check = false;
	};
};

if (_check) then {
	pmCurrentIndex = _trgIndex;

	if (!pmCurrentMarkerCreated) then {
		pmCurrentMarker = createMarker ["pmCurrentObjective", _curTown select 1];
		pmCurrentMarker setMarkerShape "ICON";
		pmCurrentMarker setMarkerType "hd_objective";
		pmCurrentMarker setMarkerColor "ColorOrange";
		pmCurrentMarkerCreated = true;
	} else {
		pmCurrentMarker setMarkerPos (_curTown select 1);
	};

	["pmCurrentTask", format ["Conquest %1", _curTown select 0], format ["Free the city of %1 from enemy occupation", _curTown select 0]] call SHK_Taskmaster_add;
	[format ["activating primary mission in %1", _curTown select 0], true] spawn LEO_fnc_log;

	[] spawn LEO_fnc_managePMAttack;
};
