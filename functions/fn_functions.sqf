
LEO_SF_vehicleMHQRespawn = {
	params ["_vehicle"];

	if ((vehicleVarName _vehicle) in MGQListStr) then {
		if (!isDedicated) then {
			[_vehicle] spawn LEO_SF_addMHQActions; // Execute before on local
		};
		
		LEO_VAR_vehicleMHQRespawn = _vehicle;
		publicVariable "LEO_VAR_vehicleMHQRespawn"; // Execute on other machines
	};
};

LEO_SF_addMHQActions = {
	params ["_vehicle"];
	_vehicle setVariable ["LEO_mhqEnabled", _vehicle getVariable ["LEO_mhqEnabled", false], true]; // To avoid some problems when respawn

	// Alert player that vehicle is disabled
	_vehicle addEventHandler ["Engine", {
		_veh = _this select 0;
		_isMhqEnabled = _veh getVariable "LEO_mhqEnabled";
		
		if (_isMhqEnabled && (player == (driver _veh))) then {
			(driver _veh) action ["engineOff", _veh];
			_veh vehicleChat "Remove the MHQ if you want to use this vehicle.";
		};
	}];

	// Teleport action
	_vehicle addAction [
		"<t color='#d96600'>Teleport</t>",	/* Title */
		"[_this] spawn LEO_fnc_teleportUnit",		/* Script */
		[],		/* Arguments */
		10,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"alive _target && _target getVariable 'LEO_mhqEnabled'"		/* Condition */
	];

	// Set as MHQ action
	_vehicle addAction [
		"<t color='#34A853'>Set as MHQ</t>",	/* Title */
		"[_this] spawn LEO_fnc_changeMHQStatus",		/* Script */
		true,		/* Arguments */
		9.1,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"alive _target && !(_target getVariable 'LEO_mhqEnabled') && (count (crew _target)) == 0"		/* Condition */
	];

	// Remove as MHQ action
	_vehicle addAction [
		"<t color='#EA4335'>Remove as MHQ</t>",	/* Title */
		"[_this] spawn LEO_fnc_changeMHQStatus",		/* Script */
		false,		/* Arguments */
		9.2,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"_target getVariable 'LEO_mhqEnabled' && (count (crew _target)) == 0"		/* Condition */
	];

	// Tent action
	_vehicle addAction [
		"<t color='#FBBC05'>Teleport to tent</t>",	/* Title */
		"[_this select 1] spawn LEO_SF_sendToTent",		/* Script */
		false,		/* Arguments */
		9.0,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"alive _target && _target getVariable 'LEO_mhqEnabled' && (groupId group _this) in (missionNamespace getVariable 'LEO_tentGroups')"		/* Condition */
	];

	if (!isNull (_vehicle getVariable ["LEO_mhqAmmoBox", objNull])) then {
		_ammoBox = _vehicle getVariable "LEO_mhqAmmoBox";
		_ammoBox addAction ["<t color='#4285F4'>Virtual Arsenal</t>", {["Open",true] spawn BIS_fnc_arsenal}];
	};
};

LEO_SF_addPlantTentAction = {
	params ["_player"];

	_player addAction [
		"<t color='#34A853'>Plant tent</t>",	/* Title */
		"[_this select 1] spawn LEO_fnc_plantTent",		/* Script */
		false,		/* Arguments */
		8,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"!((groupId group _this) in (missionNamespace getVariable 'LEO_tentGroups')) && (leader group _this) == _this"		/* Condition */
	];
};

LEO_SF_sendToTent = {
	params ["_player"];

	_group = group _player;

	_tentGroupIndex = -1;
	{
		if (_x == groupId _group) then {
			_tentGroupIndex = _forEachIndex;
		};
	} forEach (missionNamespace getVariable "LEO_tentGroups");

	_tent = missionNamespace getVariable "LEO_tentObjects" select _tentGroupIndex;
	_position = position _tent;

	titleText ["You are about to be teleported to the tent..", "BLACK", 5];
	sleep 5;
	_player setPos _position;
	titleFadeOut 1;	
}
