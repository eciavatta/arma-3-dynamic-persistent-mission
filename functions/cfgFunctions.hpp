class LEO
{
	tag = "LEO";
	class functions
	{
		file = "functions";
		class functions {description = "core functions, called on mission start."; preInit = 1;};
	};
	class objectives
	{
		file = "functions\objectives";
		class initPM {description = "";};
		class retTownList {description = "";};
		class genPMMarkers {description = "";};		
		class createPMTriggers {description = "";};
		class actPMTriggers {description = "";};
		class deactPMTriggers {description = "";};
		class managePMAttack {description = "";};
		
		class test {description = "test";};		
	};
	class spawn
	{
		file = "functions\spawn";
		class preparePMTown {description = "";};
		class spawnEnemyGroup {description = "";};
		class populateTown {description = "";};
		class generateMines {description = "";};
	};
	class teleport
	{
		file = "functions\teleport";
		class initTeleport {description = "";};
		class teleportUnit {description = "";};
		class changeMHQStatus {description = "";};
		class plantTent {description = "";};
		class removeTent {description = "";};
	};
	class utils
	{
		file = "functions\utils";
		class log {description = "";};
		class godMode {description = "";};
		class createDebugActions {description = "";};
	};
};