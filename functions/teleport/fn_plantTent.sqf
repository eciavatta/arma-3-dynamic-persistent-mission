
params ["_player"];

_group = group _player;

_tent = "Land_TentDome_F" createVehicle position _player;
_tent enableSimulationGlobal false;
_tent allowDamage false;
LEO_VAR_allowDamage = [_tent, false]; publicVariable "LEO_VAR_allowDamage";

_tent addAction [
	"<t color='#EA4335'>Remove tent</t>",	/* Title */
	"[_this select 1] spawn LEO_fnc_removeTent",		/* Script */
	false,		/* Arguments */
	8,		/* Priority */
	false,	/* ShowWindow */
	true,	/* HideOnUse */
	"",		/* Shortcut */
	"(leader group _this) == _this"		/* Condition */
];

_mrk = createMarker [format ["tentMarker%1", groupId _group], position _player];
_mrk setMarkerShape "ICON";
_mrk setMarkerType "hd_dot";
_mrk setMarkerColor "colorGreen";
_mrk setMarkerText format ["%1 Tent", groupId _group];

missionNamespace setVariable ["LEO_tentGroups", (missionNamespace getVariable "LEO_tentGroups") + [groupId _group], true];
missionNamespace setVariable ["LEO_tentObjects", (missionNamespace getVariable "LEO_tentObjects") + [_tent], true];
missionNamespace setVariable ["LEO_tentMarkers", (missionNamespace getVariable "LEO_tentMarkers") + [_mrk], true];

true
