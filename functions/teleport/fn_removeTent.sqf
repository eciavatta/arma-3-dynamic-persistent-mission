
params ["_player"];

_group = group _player;

_tentGroupIndex = -1;
{
	if (_x == groupId _group) then {
		_tentGroupIndex = _forEachIndex;
	};
} forEach (missionNamespace getVariable "LEO_tentGroups");

_tent = missionNamespace getVariable "LEO_tentObjects" select _tentGroupIndex;
deleteVehicle _tent;

_mrk = missionNamespace getVariable "LEO_tentMarkers" select _tentGroupIndex;
deleteMarker _mrk;

missionNamespace setVariable ["LEO_tentGroups", (missionNamespace getVariable "LEO_tentGroups") - [groupId _group], true];
missionNamespace setVariable ["LEO_tentObjects", (missionNamespace getVariable "LEO_tentObjects") - [_tent], true];
missionNamespace setVariable ["LEO_tentMarkers", (missionNamespace getVariable "LEO_tentMarkers") - [_mrk], true];

true
