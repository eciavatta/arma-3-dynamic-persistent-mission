
MHQList = [MHQ1, MHQ2]; // List of MHQ
MGQListStr = ["MHQ1", "MHQ2"];

// Init MHQ on server
if (isServer) then {
	missionNamespace setVariable ["LEO_respawnPoints", INS_REV_CFG_list_of_respawn_locations_blufor, true];
	{
		[[_x, nil, nil, true]] call LEO_fnc_changeMHQStatus;
	} forEach MHQList;
};

// Init MHQ on clients
if (!isDedicated) then {
	INS_REV_CFG_list_of_respawn_locations_blufor = missionNamespace getVariable "LEO_respawnPoints";

	{
		[_x] spawn LEO_SF_addMHQActions;		
	} forEach MHQList;

	// Init base teleport
	Base addAction [
		"<t color='#d96600'>Teleport</t>",	/* Title */
		"[_this] spawn LEO_fnc_teleportUnit",		/* Script */
		[],		/* Arguments */
		10,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		""		/* Condition */
	];

	// Tent action
	Base addAction [
		"<t color='#FBBC05'>Teleport to tent</t>",	/* Title */
		"[_this select 1] spawn LEO_SF_sendToTent",		/* Script */
		false,		/* Arguments */
		9.0,		/* Priority */
		false,	/* ShowWindow */
		true,	/* HideOnUse */
		"",		/* Shortcut */
		"(groupId group _this) in (missionNamespace getVariable 'LEO_tentGroups')"		/* Condition */
	];	
};

// Init Teleport on server
if (isServer) then {
	missionNamespace setVariable ["LEO_tentGroups", [], true];
	missionNamespace setVariable ["LEO_tentObjects", [], true];
	missionNamespace setVariable ["LEO_tentMarkers", [], true];
};

// Init Teleport on clients
if (!isDedicated) then {
	[player] spawn LEO_SF_addPlantTentAction;
	player addEventHandler ["Respawn", { [player] spawn LEO_SF_addPlantTentAction }];
};
