
params ["_args"];

_vehicle = _args select 0;
_status = _args select 3;

_vehicle setVariable ["LEO_mhqEnabled", _status, true];
//_vehicle enableSimulationGlobal !_status;
_vehicle allowDamage !_status;
LEO_VAR_allowDamage = [_vehicle, !_status]; publicVariable "LEO_VAR_allowDamage";

if (_status) then {
	_net = "CamoNet_BLUFOR_big_F" createVehicle position _vehicle;
	_net attachTo [_vehicle, [0, 0, 0]];
	_net setDir (getDir _vehicle + 0);
	_net enableSimulationGlobal false;	
	_net allowDamage false;
	LEO_VAR_allowDamage = [_net, false]; publicVariable "LEO_VAR_allowDamage";

	_ammoBox = "B_CargoNet_01_ammo_F" createVehicle position _vehicle;
	_ammoBox attachTo [_vehicle, [4, 6, -1.2]];
	//_ammoBox setDir (getDir _vehicle + 0);
	_ammoBox enableSimulationGlobal false;
	_ammoBox allowDamage false;
	LEO_VAR_allowDamage = [_net, _ammoBox]; publicVariable "LEO_VAR_allowDamage";
	
	_ammoBox addAction ["<t color='#4285F4'>Virtual Arsenal</t>", {["Open",true] spawn BIS_fnc_arsenal}];
	LEO_VAR_respawnVirtualArsenal = _ammoBox;
	publicVariable "LEO_VAR_respawnVirtualArsenal"; // Set virtual arsenal action for all clients

	_vehicle setVariable ["LEO_mhqNet", _net, true];
	_vehicle setVariable ["LEO_mhqAmmoBox", _ammoBox, true];

	LEO_VAR_respawnPoints = (missionNamespace getVariable "LEO_respawnPoints") + [vehicleVarName _vehicle];
	missionNamespace setVariable ["LEO_respawnPoints", LEO_VAR_respawnPoints, true];
	INS_REV_CFG_list_of_respawn_locations_blufor = LEO_VAR_respawnPoints; // In locale altrimenti non viene eseguito
	publicVariable "LEO_VAR_respawnPoints";
} else {
	_net = _vehicle getVariable "LEO_mhqNet";
	_vehicle setVariable ["LEO_mhqNet", objNull, true];
	deleteVehicle _net;

	_ammoBox = _vehicle getVariable "LEO_mhqAmmoBox";
	_vehicle setVariable ["LEO_mhqAmmoBox", objNull, true];
	deleteVehicle _ammoBox;

	LEO_VAR_respawnPoints = (missionNamespace getVariable "LEO_respawnPoints") - [vehicleVarName _vehicle];
	missionNamespace setVariable ["LEO_respawnPoints", LEO_VAR_respawnPoints, true];
	INS_REV_CFG_list_of_respawn_locations_blufor = LEO_VAR_respawnPoints; // In locale altrimenti non viene eseguito
	publicVariable "LEO_VAR_respawnPoints";
};
