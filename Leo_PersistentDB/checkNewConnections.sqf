
while { true } do {
	waitUntil { !PDB_thereAreNewConnections };

	{
		if (_x getVariable "isJip") then {
			[_x] spawn PDB_loadPlayer;

			_x setVariable ["isJip", false, true];
		}
	} forEach allPlayers;

	PDB_thereAreNewConnections = false;
};
