
#include "config.sqf";
#include "functions.sqf";

if (!isDedicated) then {
	[] spawn {
		["init player " + profileName + " on local", 3] call PDB_log;

		playerLoaded = false;
		titleText ["Wait loading of your character ..", "BLACK FADED", 30];

		player setVariable ["profileName", profileName, true];
		player setVariable ["steamName", profileNameSteam, true];
		player setVariable ["localInit", true, true];

		waitUntil { playerLoaded };
		titleText ["Loading completed", "BLACK FADED", 1];
		titleFadeOut 2;
	};
};

if (!isServer) exitWith {};

pdbHandles = [];

_result = "extDB3" callExtension "9:VERSION";
if (_result == "") then {
	diag_log "persistendb: error loading extDB3";
	exit;
};

_result = "extDB3" callExtension "9:LOCK_STATUS";
if (_result == "[0]") then {
	_result = call compile ("extDB3" callExtension "9:ADD_DATABASE:persistentdb");
	if ((_result select 0) != 1) then {
		diag_log format["persistendb error: %1", _result select 1];
		exit;
	};

	"extDB3" callExtension "9:ADD_DATABASE_PROTOCOL:persistentdb:SQL:QUERY";
	"extDB3" callExtension "9:LOCK";

	diag_log "persistendb: extDB3 loaded successfully";
} else {
	diag_log "persistendb: extDB3 already loaded";
};

addMissionEventHandler ["PlayerConnected", {
	private ["_owner"];
	_owner = _this select 4;

	[(_this select 2) + " (owner = " +  str _owner + ") is now connected", 3] call PDB_log;
	
	_pdbHandle = [_owner] spawn PDB_initPlayer;
	pdbHandles set [_owner, _pdbHandle];
}];

addMissionEventHandler ["PlayerDisconnected", {
	private ["_owner"];
	_owner = _this select 4;

	[(_this select 2) + " (owner = " +  str _owner + ") is now disconnected", 3] call PDB_log;
	_pdbHandle = pdbHandles select _owner;

	if (!scriptDone _pdbHandle) then {
		terminate _pdbHandle;
	};	
}];
