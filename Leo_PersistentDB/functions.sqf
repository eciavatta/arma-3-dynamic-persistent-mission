
PDB_log = {
	params ["_obj", ["_debugLevel", 0]];

	if (PDB_debugActivated && _debugLevel <= PDB_debugLevel) then {
		diag_log format ["persistentdb debug: %1", _obj];
	};
};

PDB_execute = {
	params ["_query"];
	_result = call compile ("extDB3" callExtension format ["0:QUERY:%1", _query]);

	if ((_result select 0) == 1) exitWith { true };

	false
};

PDB_query = {
	params ["_query"];
	_result = call compile ("extDB3" callExtension format ["0:QUERY:%1", _query]);

	if ((_result select 0) == 1) exitWith { _result select 1 };
	
	objNull
};

PDB_loadPlayer = {
	params ["_player"];

	_uid = getPlayerUID _player;
	_name = _player getVariable "profileName";
	_steam = _player getVariable "steamName";
	_owner = owner _player;

	_playerData = [format ["SELECT * FROM players WHERE playerUID = '%1'", _uid]] call PDB_query;

	if (count _playerData == 0) then { // Player is new
		_isInVehicle = if (vehicle _player != _player) then [{1}, {0}];
		_query = "INSERT INTO players (playerUID, profileName, position, direction, damage, hitPointsDamage, unitLoadout, fatigue, stamina, isInVehicle, createdDate) VALUES ";
		_query = _query + format ["('%1', '%2', '%3', %4, %5, '%6', '%7', %8, %9, %10, NOW())", _uid, str _name, getPos _player, getDir _player, damage _player, getAllHitPointsDamage _player,
								  getUnitLoadout _player, getFatigue _player, getStamina _player, _isInVehicle];
		[_query] call PDB_execute;

		["player " + _name + " initilized in database", 0] call PDB_log;
	} else { // Player already exist in db		
		_playerData = _playerData select 0;

		_playerDamage = _playerData select 5;
		if (_playerDamage < 0.999999) then {
			_isInVehicle = _playerData select 10;
			if (_isInVehicle == 0) then {
				_player setPos (_playerData select 3);
				_player setDir (_playerData select 4);
			};
			
			_player setDamage _playerDamage;

			_hitPointsDamage = _playerData select 6;
			for "_x" from 0 to count (_hitPointsDamage select 0) - 1 do {
				_player setHitPointDamage [(_hitPointsDamage select 0) select _x, (_hitPointsDamage select 2) select _x];
			};

			removeHeadgear _player;
			removeGoggles _player;
			removeVest _player;
			removeBackpack _player;
			removeUniform _player;
			removeAllWeapons _player;
			removeAllAssignedItems _player;

			_player setUnitLoadout (_playerData select 7);
			_player setFatigue (_playerData select 8);
			_player setStamina (_playerData select 9);
		};		

		["player " + _name + " restored from database", 0] call PDB_log;
	};

	playerLoaded = true;
	_owner publicVariableClient "playerLoaded";
};

PDB_updatePlayer = {	
	params ["_player"];
	_name = _player getVariable "profileName";

	_isInVehicle = if (vehicle _player != _player) then [{1}, {0}];
	_query = format ["UPDATE players SET position = '%1', direction = %2, damage = %3, hitPointsDamage = '%4', unitLoadout = '%5', fatigue = %6, stamina = %7, isInVehicle = %8",
					 getPos _player, getDir _player, damage _player, getAllHitPointsDamage _player, getUnitLoadout _player, getFatigue _player, getStamina _player, _isInVehicle];
	_query = _query + format [" WHERE playerUID = '%1'", getPlayerUID _player];

	[_query] call PDB_execute;

	["player " + _name + " updated in the database", 5] call PDB_log;
};

PDB_initPlayer = {
	params ["_owner"];
	
	waitUntil { ({owner _x == _owner} count allPlayers) == 1 }; // Wait that player is loaded on server	
	_player = (allPlayers select {owner _x == _owner}) select 0;
	waitUntil { _player getVariable ["localInit", false] }; // Wait local init

	[_player] spawn PDB_loadPlayer;

	while { true } do {
		sleep PDB_playerSaveInterval;

		if (!isNull _player && isPlayer _player) then {
			[_player] spawn PDB_updatePlayer;
		};		
	};	
};
